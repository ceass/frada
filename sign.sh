#!/bin/bash
ALIAS=Frada
KEYSTORE=$ALIAS.keystore
PWD=FradasWissco
OUTPUT_PATH=build/app/outputs
SDK_PATH=$ANDROID_SDK_ROOT
API_VER=28.0.3

if [ "$1" = "keystore" ]; then
    keytool -genkey -v -keystore $ALIAS.keystore -alias $ALIAS -keyalg RSA -keysize 2048 -validity 10000
    keytool -importkeystore -srckeystore $ALIAS.keystore -destkeystore $ALIAS.keystore -deststoretype pkcs12
elif [ "$1" = "bundle" ]; then
    flutter build appbundle
elif [ "$1" = "sign-bundle" ]; then
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE "${OUTPUT_PATH}/bundle/release/${ALIAS}-bundle.aab" $ALIAS
## this step build the signed release, not necessary to sign again
elif [ "$1" = "release" ]; then
    tns build android --release --key-store-path $KEYSTORE --key-store-password $PWD --key-store-alias $ALIAS --key-store-alias-password $PWD --copy-to "${OUTPUT_PATH}/apk/release/${ALIAS}-signed.apk"
elif [ "$1" = "sign" ]; then
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE "${OUTPUT_PATH}/apk/release/${ALIAS}-unsigned.apk" $ALIAS
elif [ "$1" = "align" ]; then
    ${SDK_PATH}/build-tools/${API_VER}/zipalign -v 4 ${OUTPUT_PATH}/apk/release/${ALIAS}-signed.apk ${OUTPUT_PATH}/apk/release/${ALIAS}-signed-aligned.apk
elif [ "$1" = "align-bundle" ]; then
    ${SDK_PATH}/build-tools/${API_VER}/zipalign -v 4 ${OUTPUT_PATH}/bundle/release/${ALIAS}-signed.aab ${OUTPUT_PATH}/bundle/release/${ALIAS}-signed-aligned.aab
fi

