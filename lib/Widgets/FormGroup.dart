import 'package:flutter/material.dart';

class FormGroup extends StatelessWidget
{
	Widget child;
	String	label;
	//TextEditingController	controller;
	
	FormGroup({this.child, this.label = null});
	
	@override
	Widget build(BuildContext context)
	{
		var childs = <Widget>[];
		if( this.label != null )
		{
			childs.add( 
				Align( 
					alignment: Alignment.topLeft, 
					child: Container( 
						child: Text(
							this.label.toUpperCase(), 
							textAlign: TextAlign.left, 
							style: TextStyle(color: Colors.red[400], fontSize:15),
						) 
					) 
				) 
			);
		}
		childs.add(this.child);
		return Container(
			margin: EdgeInsets.only(bottom: 10),
			child: Column(children: childs)
		);
	}
}
