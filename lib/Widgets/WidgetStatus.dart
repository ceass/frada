import 'package:flutter/material.dart';

class WidgetStatus extends StatelessWidget
{
	final String	status;
	
	WidgetStatus({@required this.status});
	
	@override
	Widget build(BuildContext context)
	{
		dynamic color = Colors.lightBlue[700];
		String	statusStr = 'Desconocido';
		if( this.status == 'publish' )
		{
			statusStr = 'Aceptado';
			color = Colors.green;
		}
		if( this.status == 'pending' )
		{
			statusStr = 'Pendiente';
			color = Colors.orange[300];
		}	
			
		return Align(
			alignment: Alignment.centerLeft,
			child: Container(
			padding: EdgeInsets.all(5),
			decoration: BoxDecoration(
				color: color,
				borderRadius: BorderRadius.circular(5),
			),
			child: Text(statusStr.toUpperCase(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))	
		));
	}
}