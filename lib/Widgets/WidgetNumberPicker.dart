import 'package:flutter/material.dart';

class WidgetNumberPicker extends StatefulWidget
{
	final ValueChanged<int> onQuantityChanged;
	final Color	btnColor;
	final initialValue;
	
	//WidgetNumberPicker({Key: key, this.onQuantityChanged}) : super(key: key);
	WidgetNumberPicker({this.onQuantityChanged, this.btnColor = const Color(0xff0065ab), this.initialValue = 0});
	
	@override
	_WidgetNumberPickerState	createState()
	{
		return _WidgetNumberPickerState();	
	}
}
class _WidgetNumberPickerState extends State<WidgetNumberPicker>
{
	TextEditingController _numberController;
	int _number = 0;
	Function()	onChanged;
	
	@override
	void initState()
	{
		super.initState();
		this._numberController = TextEditingController();
		this._numberController.text = this.widget.initialValue.toString();
		this._number = this.widget.initialValue;
	}
	void dispose()
	{
		this._numberController.dispose();
		super.dispose();
	}
	void _incrementNumber()
	{
		this._number++;
		this.widget.onQuantityChanged(this._number);
		setState(() 
		{
			this._numberController.text = this._number.toString();
		});
	}
	void _decrementNumber()
	{
		--this._number;
		if( this._number <= 0 )
			this._number = 0;
		this.widget.onQuantityChanged(this._number);
		setState( () 
		{
			this._numberController.text = this._number.toString();
		});
	}
	@override
	Widget build(BuildContext context)
	{
		//this._numberController.text = this._number.toString();
		return Container(
			height: 40,
			child: Row(
				children: [
					Container(
						margin: EdgeInsets.only(right:10),
						child: Container(
							decoration: ShapeDecoration(
								color: this.widget.btnColor,
								shape: CircleBorder()
							),
							child: IconButton(
								icon: Icon(Icons.remove),
								color: Colors.white,
								tooltip: 'Reducir',
								onPressed: _decrementNumber
							)
						)
					),
					Expanded( 
						child: Container(
							/*
							decoration: BoxDecoration(
								image: DecorationImage(
									image: AssetImage("images/bag.png"),
									fit: BoxFit.contain,
									alignment: Alignment.center
								)
							),
							*/
							child: TextField(
								obscureText: false,
								decoration: InputDecoration(
									border: OutlineInputBorder(),
									labelText: ''//this._number.toString(),
								),
								textAlign: TextAlign.center,
								//style: Theme.of(context).textTheme.body1,
								keyboardType: TextInputType.number,
								controller: this._numberController,
								onSubmitted: (String value)
								{
									print(value);
								}
							)
						)
					),
					Container(
						margin: EdgeInsets.only(left: 10),
						child: Container(
							decoration: ShapeDecoration(
								color: this.widget.btnColor,
								shape: CircleBorder()
							),
							child: IconButton(
								icon: Icon(Icons.add),
								color: Colors.white,
								tooltip: 'Incrementar',
								onPressed: this._incrementNumber
							)
						),
					),
				]
			)
		);
	}
}
