import 'dart:collection';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:geolocator/geolocator.dart';
import '../Classes/SB_Settings.dart';
import 'WidgetButton.dart';
import '../Classes/WPUser.dart';
import '../Services/WpService.dart';
import '../Services/ServiceUsers.dart';
import 'WidgetLoading.dart';

class WidgetMyLocation extends StatefulWidget
{
	@override
	WidgetMyLocationState	createState() => WidgetMyLocationState();
}
class WidgetMyLocationState extends State<WidgetMyLocation>
{
	WebViewController	_controller;
	String				url;
	dynamic				location;
	
	Widget getMap()
	{
		return WebView(
			initialUrl: this.url,
			javascriptMode: JavascriptMode.unrestricted,
			javascriptChannels: <JavascriptChannel>[
				JavascriptChannel(
					name: 'mob_update_location', 
					onMessageReceived: (JavascriptMessage msg)
					{
						var data = json.decode(msg.message);
						print(data);
						this._updateLocation(data);
					}
				),
			].toSet(),
			onWebViewCreated: (WebViewController webViewController)
			{
				this._controller = webViewController;
				SB_Settings.getObject('location').then( (data) 
				{
					print('Current location');
					print(data);
					this.setState(()
					{
						this.url = 'https://www.fradaswissco.com/?view=map_my_location&lat=${data['lat']}&lng=${data['lng']}';
						this._controller.loadUrl(this.url);
					});
				});
			},
		);
	}
	Widget getMapStack()
	{
		return Stack(
			children: [
				Positioned(
					top:0,
					right: 0,
					bottom:0,
					left: 0,
					child: Container(
						child: this.getMap() 
					)
				),
			]
		);
	}
	void _getCurrentLocation()
	{
		getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
			.then( (position) 
			{
				var location = {
					'lat': position.latitude.toString(), 
					'lng': position.longitude.toString()
				};
				this._updateLocation(location);
				this.url = 'https://www.fradaswissco.com/?view=map_my_location&lat=${position.latitude.toString()}&lng=${position.longitude.toString()}';
				this.setState(()
				{
					this._controller.loadUrl(this.url);
				});
				
			})
			.catchError( (e)
			{
				print("Get position error");
				print(e);
			});
	}
	void _updateLocation(Map location) async
	{
		SB_Settings.saveObject('location', location);
		ServiceUsers.updateLocation(location);
		/*
		Scaffold.of(this.context).showSnackBar(
			SnackBar(content: Text('Ubicación actualizada')),
		);
		*/
	}
	@override
	void initState()
	{
		super.initState();
		/*
		isLocationServiceEnabled().then((isLocationEnabled)
		{
			print('LocationEnabled: ${isLocationEnabled}');
		});
		*/
		checkPermission().then((perms)
		{
			if( /*perms == LocationPermission.whileInUse*/ perms != LocationPermission.always )
			{
				showDialog(
					context: this.context,
					builder: (_)
					{
						return AlertDialog(
							title: Text('Permisos requeridos'),
							content: SingleChildScrollView(
								child: ListBody(
									children: <Widget>[
										Text('La aplicacion necesita obtener los permisos para acceder a su ubicación.'),
										SizedBox(height: 10),
										Text('Desea acceder a la configuración de su teléfono para asignar los permisos correspondientes?'),
									],
								),
							),
							actions: [
								FlatButton(
									child: Text('No', style: TextStyle(color: Colors.white)),
									color: Color(0xffFF4C58),
									onPressed: ()
									{
										Navigator.pop(context);
									}
								),
								FlatButton(
									child: Text('Si', style: TextStyle(color: Colors.white)),
									color: Color(0xffFF4C58),
									onPressed: ()
									{
										Navigator.pop(context);
										openAppSettings();
										openLocationSettings();
									}
								)
							]
						);
					}
				);
			}
		});
		/*
		requestPermission().then((perms)
		{
			print('requestPermissions');
			print(perms);
		});
		*/
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			resizeToAvoidBottomInset: false,
			appBar: AppBar(
				title: Text('Establecer tu ubicación', style: TextStyle(color: Colors.white)),
				iconTheme: IconThemeData(color: Colors.white),
				actions: <Widget>[
					IconButton(
						icon: Icon(Icons.my_location),
						tooltip: 'Obtener ubicación actual',
						onPressed: ()
						{
							this._getCurrentLocation();
						},
					)
				],
			),
			body: Container(
				child: this.getMapStack()
			)
		);
	}
}