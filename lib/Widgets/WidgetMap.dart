import 'dart:collection';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../Classes/SB_Settings.dart';
import 'WidgetButton.dart';
import '../Classes/WPUser.dart';
import '../Services/WpService.dart';
import 'WidgetLoading.dart';

class WidgetMap extends StatefulWidget
{
	@override
	WidgetMapState	createState() => WidgetMapState();
}
class WidgetMapState extends State<WidgetMap>
{
	WebViewController	_controller;
	String				url;
	dynamic				location;
	
	Widget getMap()
	{
		print(this.url);
		return WebView(
			initialUrl: this.url,
			javascriptMode: JavascriptMode.unrestricted,
			javascriptChannels: <JavascriptChannel>[
				JavascriptChannel(
					name: 'mob_position', 
					onMessageReceived: (JavascriptMessage msg)
					{
						var data = json.decode(msg.message);
						this.location = {
							'lat': data[0],
							'lng': data[1],
							'address': 'Some address from map'
						};
					}
				),
				JavascriptChannel(
					name: 'mob_select_consuntant',
					onMessageReceived: (JavascriptMessage msg)
					{
						var data = json.decode(msg.message);
						print(msg.message);
						if( data['ID'] != null )
						{
							final _key = GlobalKey<State>();
							WidgetLoading.show(context, _key, 'Asignando consultora..');
							WpService.getConsultant(int.parse(data['ID'].toString()))
								.then( (WPUser user)
								{
									WidgetLoading.hide(_key);
									Navigator.pop(context, user);
								})
								.catchError((e)
								{
									WidgetLoading.hide(_key);
									print(e);
								});
							
						}
					}
				)
			].toSet(),
			onWebViewCreated: (WebViewController webViewController)
			{
				this._controller = webViewController;
				SB_Settings.getObject('location').then( (data) 
				{
					print(data);
					this.setState(()
					{
						this.url = 'https://www.fradaswissco.com/?view=map_consultoras&lat=${data['lat']}&lng=${data['lng']}';
						this._controller.loadUrl(this.url);
					});
				});
			},
		);
	}
	Widget getMapStack()
	{
		return Stack(
			children: [
				Positioned(
					top:0,
					right: 0,
					bottom:0,
					left: 0,
					child: Container(
						child: this.getMap() 
					)
				),
				/*
				Positioned(
					right:0,
					bottom: 20,
					left:0,
					child: Container(
						padding: EdgeInsets.only(right:20,left:20),
						child: Center(
							child: WidgetButton(
								text: 'Utilizar dirección', type: 'primary',
								callback: ()
								{
									SB_Settings.saveObject('current_address', this.location)
										.then( (_) 
										{
											Navigator.pop(this.context);
										});
								},
							)
						)
					)
				)
				*/
			]
		);
	}
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			resizeToAvoidBottomInset: false,
			
			//extendBodyBehindAppBar: true,
			appBar: AppBar(
				//toolbarOpacity: 1,
				//backgroundColor: Colors.transparent,
				//elevation: 0,
				title: Text('Busca tu Dirección', style: TextStyle(color: Colors.white)),
				iconTheme: IconThemeData(color: Colors.white),
			),
			body: Container(
				child: this.getMapStack()
			)
		);
	}
}