import 'package:flutter/material.dart';
import '../../Classes/WpPost.dart';
import 'WidgetService.dart';

class WidgetServiceGridItem extends StatelessWidget
{
	final	WpPost	service;
	Color			color;
	
	WidgetServiceGridItem({this.service, this.color: const Color(0xff000000)});
	
	@override
	Widget build(BuildContext context)
	{
		var size = MediaQuery.of(context).size;
		
		return Container(
			padding: EdgeInsets.all(9),
			/*
			decoration: BoxDecoration(
				image: DecorationImage(
					image: NetworkImage(this.service.getThumbnail()),
					fit: BoxFit.cover,
				)
			),
			*/
			child: InkWell(
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.center,
					mainAxisAlignment: MainAxisAlignment.center,
					children: [
						Container(
							width: size.width > 800 ? 140 : 90,
							height: size.width > 800 ? 140 : 90,
							decoration: BoxDecoration(
								color: this.color,
								borderRadius: BorderRadius.circular(size.width > 800 ? 80 : 50),
								image: DecorationImage(
									image: this.service.getThumbnail().isNotEmpty ? 
										NetworkImage(this.service.getThumbnail()) : 
										AssetImage('images/placeholder.jpg'),
									fit: BoxFit.cover,
								),
							),
						),
						SizedBox(height: 10),
						Text(this.service.title, style: TextStyle(fontSize: 15))
					]
				),
				onTap: ()
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetService(service: this.service,)));
				},
			)
		);
	}
}