import 'package:flutter/material.dart';
import 'dart:async';
import '../../Services/WpService.dart';
import '../../Classes/WPUser.dart';
import 'WidgetConsultantProfile.dart';

class WidgetSearchConsultant extends StatefulWidget
{
	@override
	_WidgetSearchConsultant	createState() => _WidgetSearchConsultant();
}
class _WidgetSearchConsultant extends State<WidgetSearchConsultant>
{
	TextEditingController	_searchController = TextEditingController();
	Widget					_widgetResult;
	Timer					_timer;
	List<WPUser>			_consultants;
	
	void _setProcessing()
	{
		this.setState(()
		{
			this._widgetResult = Column(
				children: [
					Text('Buscando...'),
					SizedBox(height: 8),
					CircularProgressIndicator()
				]
			);
		});
		
	}
	Widget _buildConsultants(List<WPUser> profiles)
	{
		var size = MediaQuery.of(this.context).size;
		print('Width: ${size.width}');
		double spacing = 6;
		double aWidth = size.width - 20 - (spacing * 2);
		int column = 3;
		double height = 200;
		if( size.width > 600 )
		{
			column = 6;
			height = 230;
		}
		
		double width = (aWidth / column).toInt().toDouble();
		
		print('profile width: ${width}');
		return Wrap(
			spacing: spacing,
			children: profiles.map((WPUser profile)
			{
				return Card(
					margin: EdgeInsets.only(bottom: spacing),
					child: Container(
						width: width,
						height: height,
						child: InkWell(
							child: Column(
								crossAxisAlignment: CrossAxisAlignment.stretch,
								children: [
									Container(
										//height: height * 0.6,
										child: Expanded(
											child: Image.network(profile.avatar, fit: BoxFit.cover)
										)
									),
									SizedBox(height: 8),
									Container(
										child: Column(
											children: [
												Text(profile.fullname),
												SizedBox(height: 8),
												Text('Distancia: -')
											]
										)
									),
									SizedBox(height: 8),
									Container(
										padding: EdgeInsets.all(5),
										child: FlatButton(
											color: Color(0xffd8d4d0),
											child: Text('Ver Perfil'),
											onPressed: ()
											{
												Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetConsultantProfile(consultant: profile)));
											},
										)
									)
								]
							),
							onTap: ()
							{
								Navigator.pop(context, profile);
							}
						)
					)
				);
			}).toList()
		);
	}
	void _keywordChanged(String value)
	{
		if( this._timer != null )
			this._timer.cancel();
		this._setProcessing();
		this._timer = Timer(Duration(milliseconds: 500), ()
		{
			print('Searching => ' + value);
			WpService.getConsultants(value)
				.then( (data) 
				{
					/*
					this._consultants = data;
					var _list = ListView.builder(
						itemCount: this._consultants.length,
						itemBuilder: (BuildContext ctx, int index)
						{
							var consultant = (this._consultants[index] as WPUser);
							return InkWell(
								child: Card(
									child: Row(
										children: [
											Container(
												width: 90,
												height: 90,
												child: ClipRRect(
													borderRadius: BorderRadius.circular(80),
													child: consultant.avatar.isNotEmpty ? 
														Image.network(consultant.avatar, fit: BoxFit.cover,) :
														Image.asset('images/woman.png', fit: BoxFit.cover,) 
												)
											),
											SizedBox(width: 20),
											Expanded(
												child: Column(
													crossAxisAlignment: CrossAxisAlignment.stretch,
													children: <Widget>[
														Text(consultant.display_name),
														SizedBox(height: 8),
														Text('Dirección: ' + consultant.address)
													],
												)
											)
										]
									)
								),
								onTap: ()
								{
									print('tap');
									Navigator.pop(context, consultant);
								},
							);
						}
					);
					*/
					this.setState( () 
					{
						this._widgetResult = this._buildConsultants(data);
					});
					
				})
				.catchError( (error)
				{
					print('ERROR');
					print(error);
				});
		});	
	}
	@override
	void initState()
	{
		super.initState();
		this._widgetResult = Container();
		WpService.getConsultants()
			.then( (data) 
			{
				this.setState(()
				{
					this._widgetResult = this._buildConsultants(data);
				});
			});
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: Text('Búsqueda de consultoras'),
				actions: [
					
				]
			),
			body: Container(
				margin: EdgeInsets.all(10),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Container(
							child: TextField(
								decoration: InputDecoration(
									hintText: 'Escriba el nombre del producto',
								),
								style: Theme.of(context).textTheme.body1,
								controller: this._searchController,
								onChanged: this._keywordChanged,
							)
						),
						Container(
							margin: EdgeInsets.all(10),
							child: Text(
								'Resultados de Busqueda', 
								style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)
							)
						),
						Expanded(
							child: ListView(
								children: [this._widgetResult]
							)
						),
					]
				)
			)
		);
	}
}