import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter_html/flutter_html.dart';
import '../../Classes/WpPost.dart';
import '../WidgetButton.dart';
import '../WidgetBottomMenu.dart';
import 'WidgetSearchConsultant.dart';
import '../../Classes/WPUser.dart';
import '../../Services/WpService.dart';
import '../../Classes/WPAppointment.dart';
import '../../Services/ServiceUsers.dart';
import '../Users/WidgetLogin.dart';
import '../WidgetLoading.dart';
import '../WidgetMap.dart';
import '../../Classes/SB_Settings.dart';
import '../FormGroup.dart';

class WidgetService extends StatefulWidget
{
	final	WpPost	service;
	WidgetService({this.service});
	
	@override
	_WidgetServiceState	createState() => _WidgetServiceState();
}
class _WidgetServiceState extends State<WidgetService>
{
	DateTime 	_currentDate;
	DateTime	_currentTime;
	DateTime	_serviceDate;
	WPUser		_consultant;
	TextEditingController	_ctrlPhone 		= TextEditingController();
	TextEditingController	_ctrlAddress	= TextEditingController();
	final					_formKey 		= GlobalKey<FormState>();
	 
	Widget _getCalendar(setter)
	{
		return CalendarCarousel<Event>(
			minSelectedDate: DateTime.now(),
			thisMonthDayBorderColor: Colors.grey,
			height: 400,
			weekFormat: false,
			selectedDateTime: this._currentDate,
			daysHaveCircularBorder: null,
			locale: 'es',
			weekendTextStyle: TextStyle(color: Colors.red, ),
			todayTextStyle: TextStyle(color: Colors.white),
			todayButtonColor: Color(0xff9f9a96),
			//weekdayTextStyle: TextStyle(decoration: BoxDecoration(border: null)),
			onDayPressed: (DateTime date, List events)
			{
				print(date);
				if( date != null )
				{
					setter(()
					{
						this._currentDate = date;
					});
					this.setState(()
					{
						this._currentDate = date;
					});
				}
				
			},
		);
	}
	List<Widget> _getTimes()
	{
		var times = <Widget>[];
		for(int i = 7; i <= 20; i++)
		{
			String hour = i < 10 ? '0${i}' : i.toString();
			times.add(
				Container(
					decoration: BoxDecoration(
						border: Border.all(width: 1, color: Color(0xffFF4C58))
					),
					child: FlatButton(
						//color: Colors.black,
						focusColor: Colors.black,
						highlightColor: Colors.red,
						child: Text('${hour}:00', style: TextStyle(color: Colors.black)),
						onPressed: ()
						{
							
						},
					)
				),
			);
			times.add(
				Container(
					decoration: BoxDecoration(
						border: Border.all(width: 1, color: Color(0xffFF4C58))
					),
					child: FlatButton(
						//color: Colors.black,
						focusColor: Colors.black,
						highlightColor: Colors.red,
						child: Text('${hour}:30', style: TextStyle(color: Colors.black)),
						onPressed: ()
						{
							
						},
					)
				),
			);
		}
		return times;
	}
	void _showSelectDate() async
	{
		final date = await showDatePicker(
			context: this.context,
			firstDate: DateTime(1900),
			initialDate: this._serviceDate ?? DateTime.now(),
			lastDate: DateTime(2100),
			locale: Locale('es', 'ES'),
			
		);
		if( date != null )
		{
			final time = await showTimePicker(
				context: this.context, 
				initialTime: TimeOfDay.fromDateTime(this._serviceDate ?? DateTime.now()),
			);
			this.setState(()
			{
				this._serviceDate = DateTimeField.combine(date, time);
				print(this._serviceDate);
			});
			
		}
		/*
		showModalBottomSheet(isScrollControlled: true, context: context, builder: (ctx) 
		{
			return SafeArea(
				child: StatefulBuilder(
					builder: (ctx, setter) 
					{
						return Container(
							height: 520,
							color: Color(0xFF737373),
							child: Container(
								//height: 450,
								decoration: BoxDecoration(
									color: Theme.of(context).canvasColor,
									borderRadius: BorderRadius.only(
										topLeft: Radius.circular(10),
										topRight: Radius.circular(10),
									),
								),
								child: Column(
									//crossAxisAlignment: CrossAxisAlignment.stretch,
									children: [
										Container(
											padding: EdgeInsets.all(8),
											child: Text('Seleccionar fecha para el servicio', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
										),
										this._getCalendar(setter),
										Container(
											padding: EdgeInsets.all(10),
											child: WidgetButton(text: 'Guardar', 
											callback: ()
											{
												Navigator.pop(context);
												this._showSelectTime();
											})
										),
									]
								)
							)
						
						);
					},
				)
			);
		});
		*/
	}
	void _showSelectTime()
	{
		
	}
	void _sendAppointment() async
	{
		if( !(await ServiceUsers.isLoggedIn()) )
		{
			Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetLogin()));
			return;
		}
		if( !this._formKey.currentState.validate() )
			return;
		try
		{
			if( this._consultant == null )
				throw new Exception('Debe seleccionar una consultora para el servicio');
			if( this._serviceDate == null )
				throw new Exception('Debe seleccionar una fecha y hora para el servicio');
			var location 		= await SB_Settings.getObject('location');
			var ap 				= new WPAppointment();
			ap.object_id 		= this.widget.service.id;
			ap.init_date		= this._serviceDate;
			ap.price			= 0;
			ap.notes			= '';
			ap.address			= this._ctrlAddress.text.trim();
			ap.phone			= this._ctrlPhone.text.trim();
			ap.assigned_user_id	= this._consultant.ID;
			print(location);
			if( location != null )
			{
				ap.latitude			= location['lat'] ?? '';
				ap.longitude		= location['lng'] ?? '';
			}
			var _key = GlobalKey<State>();
			WidgetLoading.show(context, _key, 'Procesando datos...');
			WpService.sendAppointment(ap)
				.then((newAp)
				{
					WidgetLoading.hide(_key);
					showDialog(
						context: context,
						builder: (_)
						{
							return AlertDialog(
								title: Text('Cita Registrada'),
								content: SingleChildScrollView(
									child: ListBody(
										children: <Widget>[
											Center(child: Image.asset('images/success-img.png')),
											SizedBox(height: 10),
											Text('Faclicidades!!'),
											SizedBox(height: 10),
											Text('Su solicitud de servicio ha sido recibida correctamente, nos comunicaremos con usted para confirmar el servicio.'),
											SizedBox(height: 10),
											FlatButton(
												child: Text('Continuar', style: TextStyle(color: Colors.white)),
												color: Color(0xffFF4C58),
												onPressed: ()
												{
													Navigator.pop(context);
												}
											)
										],
									),
								),
								/*
								actions: [
									FlatButton(
										child: Text('Cerrar', style: TextStyle(color: Colors.white)),
										color: Color(0xffFF4C58),
										onPressed: ()
										{
											Navigator.pop(context);
										}
									)
								]
								*/
							);
						}
					);
				})
				.catchError((error)
				{
					WidgetLoading.hide(_key);
					print(error);
					throw error;
				});
		}
		catch(e)
		{
			showDialog(
				context: context,
				builder: (_)
				{
					return AlertDialog(
						title: Text('Error al agendar cita'),
						content: SingleChildScrollView(
							child: ListBody(
								children: <Widget>[
									Text(e.toString()),
								],
							),
						),
						actions: [
							FlatButton(
								child: Text('Cerrar'),
								onPressed: ()
								{
									Navigator.pop(context);
								}
							)
						]
					);
				}
			);
		}
	}
	Future<Map> _isOpen() async
	{
		var settings = await SB_Settings.getObject('settings');
		bool open = true;
		print(settings['business_hours']);
		if( settings['business_hours'] != null && settings['business_hours']['from'] != null && settings['business_hours']['to'] != null )
		{
			print('Validate hours');
			var cdate 	= DateTime.now();
			var pfrom	= settings['business_hours']['from'].toString().split(':');
			var pto		= settings['business_hours']['to'].toString().split(':');
			
			int fhour 	= int.parse(pfrom[0].toString().padLeft(1, '0'));
			int fmin	= int.parse(pfrom[1].toString().padLeft(1, '0'));
			int thour 	= int.parse(pto[0].toString().padLeft(1, '0'));
			int tmin	= int.parse(pto[1].toString().padLeft(1, '0'));
			var from = DateTime(cdate.year, cdate.month, cdate.day, fhour, fmin);
			var to = DateTime(cdate.year, cdate.month, cdate.day, thour, tmin);
			
			if( cdate.millisecondsSinceEpoch >= from.millisecondsSinceEpoch && cdate.millisecondsSinceEpoch <= to.millisecondsSinceEpoch )
			{
				open = true;
			}
			else
			{
				open = false;
			}
		}
		
		var data = {
			'hours': settings['business_hours'] ?? null,
			'open': open
		};
		return data;
	}
	Widget _bookingForm()
	{
		return Column(
			children: [
				Text('Solicitud del Servicio', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
				SizedBox(height: 10),
				Container(
					padding: EdgeInsets.all(10),
					color: Color(0xffffd6d1),
					child: Row(
						children: [
							ClipRRect(
								borderRadius: BorderRadius.circular(80),
								child: Container(
									color: Colors.white,
									child: this._consultant != null && this._consultant.avatar.isNotEmpty ? 
											Image.network(this._consultant.avatar, width: 80, height: 80) : 
											Image.asset('images/woman.png', width: 80, height: 80)
								),
							),
							SizedBox(width: 10),
							Expanded(
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									children: [
										Text('Manicurista', style: TextStyle(fontWeight: FontWeight.bold)),
										SizedBox(height: 10),
										Text(this._consultant != null ? this._consultant.display_name : 'No asignada'),
										SizedBox(height: 10),
										Text('Dirección', style: TextStyle(fontWeight: FontWeight.bold)),
										SizedBox(height: 10),
										Text(this._consultant != null ? this._consultant.address : 'Sin dirección')
									]
								)
							),
							Tooltip(
								message: 'Presiona para buscar a tu manicurista preferido',
								child: PopupMenuButton(
									itemBuilder: (ctx)
									{
										return [
											PopupMenuItem(
												value: 'map',
												child: Row(
													children:[
														Icon(Icons.location_on),
														ClipRect(
															child: Text('Buscar por ubicación'),
														)
													]
												),
											),
											PopupMenuItem(
												value: 'name',
												child: Row(
													children: [
														Icon(Icons.search),
														Text('Buscar por nombre'),
													]
												),
											)
										];
									},
									onSelected: (result) async
									{
										print(result);
										if( result == 'map' )
										{
											var consultant = await Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetMap() ));
											this.setState(()
											{
												this._consultant = consultant;
											});
										}
										else if( result == 'name' )
										{
											var consultant = await Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetSearchConsultant()));
											this.setState(()
											{
												this._consultant = consultant;
											});
										}
									}
								)
							)
						]
					)
				),
				SizedBox(height: 10),
				Text('Fecha y hora', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
				Row(
					children: [
						Expanded(
							child: Container(
								padding: EdgeInsets.all(5),
								child: Text(
									this._serviceDate != null ? DateFormat('dd-MM-yyyy HH:mm').format(this._serviceDate) : 'No seleccionado'
								)
							)
						),
						FlatButton(
							child: Text('Seleccionar'),
							onPressed: ()
							{
								this._showSelectDate();
							},
						)
					]
				),
				SizedBox(height: 10),
				Text('Información Adicional', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
				Container(
					padding: EdgeInsets.all(8),
					child: Form(
						key: this._formKey,
						child: Column(
							children: [
								FormGroup(
									label: 'Teléfono de contacto',
									child: TextFormField(
										controller: this._ctrlPhone,
										validator: (val)
										{
											if( val.isEmpty )
												return 'Debe ingresar un numero de telefono de referencia';
										},
									)
								),
								FormGroup(
									label: 'Dirección',
									child: TextFormField(
										maxLines: 5,
										controller: this._ctrlAddress,
									)
								),
							]
						),
					)
				),
				FlatButton(
					color: Color(0xffd8d4d0),
					//textColor: Color(0xff9f9a96),
					child: Text('Agendar servicio'),
					onPressed: ()
					{
						this._sendAppointment();
					}
				)
			]
		);
	}
	@override
	Widget build(BuildContext context)
	{
		String color = this.widget.service.color != null ? this.widget.service.color.replaceFirst('#', 'ff') : 'ff000000';
		var size	= MediaQuery.of(context).size;
		
		return Scaffold(
			appBar: AppBar(title: Text(this.widget.service.title)),
			bottomNavigationBar: WidgetBottomMenu(currentIndex: 2,),
			body: SafeArea(
				child: Container(
					child: ListView(
						children: <Widget>[
							Container(
								height: size.height * 0.25,
								color: Color(int.parse(color, radix: 16)),
								child: this.widget.service.getThumbnail().isNotEmpty ? 
									Image.network(this.widget.service.getThumbnail(), fit: BoxFit.cover,) :
									Image.asset('images/placeholder.jpg', fit: BoxFit.cover)
							),
							SizedBox(height: 10),
							Container(
								child: Column(
									children: [
										Html(data: this.widget.service.content),
										SizedBox(height: 10),
										Divider(height: 2,),
										SizedBox(height: 10),
										FutureBuilder(
											future: this._isOpen(),
											builder: (ctx, snapshot)
											{
												if( snapshot.hasData )
												{
													if( snapshot.data['open'] )
													{
														return this._bookingForm();
													}
													else
													{
														return Center(
															child: Text(
																'Realize su reserva en horarios de oficina\n${snapshot.data['hours']['from']} - ${snapshot.data['hours']['to']}',
																textAlign: TextAlign.center, 
																style: TextStyle(
																	color: Colors.red,
																	fontSize: 20
																)
															)
														);
													}
												}
												return SizedBox(height: 0);
											},
										),
									]
								)
							),
							SizedBox(height: 10),
							
						],
					)
				),
			)
		);
	}
}