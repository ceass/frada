import 'package:flutter/material.dart';
import '../../Classes/WpPost.dart';
import 'WidgetServiceGridItem.dart';
import '../../Services/WpService.dart';
import '../WidgetBottomMenu.dart';

class WidgetServices extends StatefulWidget
{
	@override
	_WidgetServicesState	createState() => _WidgetServicesState();
}
class _WidgetServicesState extends State<WidgetServices>
{
	List<WpPost>		_services = [];
	
	void _loadServices()
	{
		/*
		this._services = [
			SB_Service(
				id: null, 
				name: 'Uñas Esculturales en Gel', 
				description: 'A tan solo 250 bs la primera vez. (un solo color + más un pequeño diseño en un dedo)',
				price: 250,
				image: 'https://www.fradaswissco.com/wp-content/uploads/2019/06/frada-nuestros-servicios-unas-esculturales-en-gel-icon.png'
			),
			SB_Service(
				id: null, 
				name: 'Pintado de Uñas en Gel', 
				description: 'A tan solo 120 Bs. Un pintado duradero al menos por 3 semanas!',
				price: 120,
				image: 'https://www.fradaswissco.com/wp-content/uploads/2019/06/frada-nuestros-servicios-pintado-unas-en-gel-icon.png',
				color: '#2baab1'
			),
			SB_Service(
				id: null, 
				name: 'Retoque', 
				description: 'A tan solo 200 bs. Para mantener tus uñas en gel en perfecto estado y cambiar de diseño!',
				price: 200,
				image: 'https://www.fradaswissco.com/wp-content/uploads/2019/06/frada-nuestros-servicios-retoque-icon.png',
				color: '#ffd3d0'
			),
			SB_Service(
				id: null, 
				name: 'Reconstrucción de Uñas', 
				description: 'A tan solo 150bs. Uñas quebradas o maltratadas de Pies + spa de pies + esmaltado en gel! *incluye sólo las dos uñas grandes.',
				price: 150,
				image: 'https://www.fradaswissco.com/wp-content/uploads/2019/06/frada-nuestros-servicios-reconstruccion-de-unas-icon.png',
				color: '#383f48'
			),
		];
		*/
		WpService.getServices().then( (services) 
		{
			this.setState(()
			{
				this._services = services;
			});
			
		});
	}
	@override
	void initState()
	{
		super.initState();
		this._loadServices();
	}
	@override
	Widget build(BuildContext context)
	{
		var size = MediaQuery.of(context).size;
		int axisCount = size.width > 800 ? 3 : 2;
		return Scaffold(
			bottomNavigationBar: WidgetBottomMenu(currentIndex: 2),
			body: Container(
				child: CustomScrollView(
					slivers: [
						SliverAppBar(
							title: Text('Servicios'),
							backgroundColor: Colors.transparent,
							expandedHeight: 200,
							flexibleSpace: FlexibleSpaceBar(
								background: Image.asset('images/products-bg.jpg', fit: BoxFit.cover,)
							)
						),
						SliverGrid(
							gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: axisCount, childAspectRatio: 1.0),
							delegate: SliverChildBuilderDelegate((ctx, index)
							{
								String color = this._services[index].color.isNotEmpty ? this._services[index].color.replaceFirst('#', 'ff') : 'ff000000';
								return WidgetServiceGridItem(service: this._services[index], color: Color(int.parse(color, radix: 16)));
							}, childCount: this._services.length)						
						),
						
					]
				),
			),
		);
	}
}