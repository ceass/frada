import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import '../../Classes/WPUser.dart';

class WidgetConsultantProfile extends StatefulWidget
{
	final WPUser	consultant;
	WidgetConsultantProfile({this.consultant});
	
	@override
	_WidgetConsultantProfileState	createState() => _WidgetConsultantProfileState();
}
class _WidgetConsultantProfileState extends State<WidgetConsultantProfile>
{
	_WidgetConsultantProfileState();
	@override
	void initState()
	{
		super.initState();
	}
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Perfil de Consultor')),
			body: SafeArea(
				child: Container(
					child: ListView(
						children: [
							Center(
								child: Container(
									width: 200,
									height: 200,
									padding: EdgeInsets.all(3),
									decoration: BoxDecoration(
										borderRadius: BorderRadius.all(Radius.circular(100.0)),
										color: Colors.white,
										border: Border.all(color: Colors.grey[300]),
									),
									child: ClipRRect(
										borderRadius: BorderRadius.circular(100),
										child: Image.network(this.widget.consultant.avatar),
									)
								)
							),
							SizedBox(height: 10),
							Card(
								child: Container(
									padding: EdgeInsets.all(10),
									child: Column(
										crossAxisAlignment: CrossAxisAlignment.stretch,
										children: [
											SizedBox(height: 10),
											Text(this.widget.consultant.fullname, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold)),
											SizedBox(height: 10),
											Html(data: this.widget.consultant.description,)
										]
									)
								) 
							),
							SizedBox(height: 10),
							Card(
								child: Container(
									padding: EdgeInsets.all(10),
									child: Column(
										crossAxisAlignment: CrossAxisAlignment.stretch,
										children: [
											SizedBox(height: 10),
											Row(
												children: [
													Text('Ciudad:', style: TextStyle(fontWeight: FontWeight.bold)),
													SizedBox(width: 10),
													Expanded(child: Text(this.widget.consultant.city))
												]
											)
										]
									)
								) 
							)
							
						]
					)
				),
			),
		);
	}
}