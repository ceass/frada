import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:provider/provider.dart';
import '../../Classes/WPUser.dart';
import '../FormGroup.dart';
import '../WidgetButton.dart';
import '../../Classes/SB_Settings.dart';
import '../../Classes/SB_Geo.dart';
import '../../Classes/SB_Cart.dart';
import '../../Classes/WCOrder.dart';
import '../../Classes/SB_Globals.dart';
import '../../Classes/WCShippingZone.dart';
import '../../Classes/WCShippingMethod.dart';
import '../../Services/WooService.dart';
import '../../Services/ServiceUsers.dart';
import '../WidgetLoading.dart';
import 'WidgetOrderSuccess.dart';
import '../../Providers/CartProvider.dart';
import '../Products/WidgetProducts.dart';

class WidgetCheckout extends StatefulWidget
{
	@override
	WidgetCheckoutState		createState() => WidgetCheckoutState();
}
class WidgetCheckoutState extends State<WidgetCheckout>
{
	SB_Cart					_cart = SB_Cart();
	TextEditingController	_ctrlName;
	TextEditingController	_ctrlEmail;
	TextEditingController	_ctrlPhone;
	TextEditingController	_ctrlAddress;
	TextEditingController	_ctrlDateTime;
	TextEditingController	_ctrlPaymentMethod	= TextEditingController();
	String					_paymentMethod;
	final					_formKey = GlobalKey<FormState>(); 
	Map						gdata;
	WPUser					user;
	DateFormat				dateFormat = DateFormat('dd-MM-yyyy HH:mm');
	WCOrder					order;
	List					_zones = <WCShippingZone>[];
	List					_shippingMethods = <WCShippingMethod>[];
	bool					_enableShippingDate = false;
	WCShippingMethod		_shippingMethod;
	
	void _buildOrder()
	{
		this.order 							= new WCOrder.fromCart(this._cart);
		this.order.set_paid					= false;
		this.order.customer_id				= this.user.ID;
		this.order.customer_note			= '';
		this.order.payment_method 			= this._paymentMethod; //'lckout';
		this.order.payment_method_title 	= 'Livees Checkout';
		this.order.billing['first_name']	= this.user.fullname;
		this.order.billing['last_name']		= '';
		this.order.billing['address_1']		= this._ctrlAddress.text.trim();
		this.order.billing['address_2']		= '';
		this.order.billing['city']			= '';
		this.order.billing['state']			= '';
		this.order.billing['postcode']		= '00000';
		this.order.billing['country']		= 'BO';
		this.order.billing['email']			= this._ctrlEmail.text.trim();
		this.order.billing['phone']			= this._ctrlPhone.text.trim();
		
		this.order.shipping['first_name']	= this.order.billing['first_name'];
		this.order.shipping['last_name']	= this.order.billing['last_name'];
		this.order.shipping['address_1']	= this.order.billing['address_1'];
		this.order.shipping['address_2']	= this.order.billing['address_2'];
		this.order.shipping['city']			= this.order.billing['city'];
		this.order.shipping['state']		= this.order.billing['state'];
		this.order.shipping['postcode']		= this.order.billing['postcode'];
		this.order.shipping['country']		= this.order.billing['country'];
		/*
		if( this.user != null )
		{
			order.customer_id 	= 0;
			order.user_id		= this.user.user_id;
		}
		*/
		if( this.gdata != null )
		{
			order.meta_data.addAll([
				{'key': '_lat', 'value': this.gdata['lat']},
				{'key': '_lng', 'value': this.gdata['lng']}
			]);
		}
		/*
		order.meta['_zona']				= this.zonaId.toString();
		order.meta['_barrio'] 			= this.barrioId.toString();
		order.meta['_shipping_address'] = this._ctrlAddress.text.trim();
		order.meta['_payment_method']	= this._paymentMethod;
		order.customer_phone			= this._ctrlPhone.text.trim();
		order.delivery_date				= this._ctrlDateTime.text.trim();
		order.shipping_cost				= this._cart.getDeliveryCost();
		*/
	}
	void _sendOrder(context)
	{
		
		if( !this._formKey.currentState.validate() )
			return;
		if( this._radioMethod == -1 )
		{
			var alert = AlertDialog(
				title: Text("Erro de datos"),
				content: Text("Debe seleccionar el lugar para el envio."),
				actions: <Widget>[
					FlatButton(
						child: Text("Ok"), 
						onPressed: ()
						{
							Navigator.of(context).pop();
						}
					)
				]
			);
			showDialog(
				context: this.context,
				builder: (ctx)
				{
					return alert;
				}
			);
			return;
		}
		/*
		
		*/
		
		this._buildOrder();
		final GlobalKey<State> _key = GlobalKey<State>();
		WidgetLoading.show(context, _key, 'Procesando su pedido...');
		WooService.sendOrder(this.order)
			.then( (newOrder) 
			{
				WidgetLoading.hide(_key);
				//showDialog(context: context, builder: (BuildContext ctx){return alert;});
				//##clear shopping cart
				this._cart.clear();
				Provider.of<CartProvider>(context, listen: false).quantity = 0;
				Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetOrderSuccess(order: newOrder)));
			})
			.catchError( (e) 
			{
				WidgetLoading.hide(_key);
				print('ORDER ERROR');
				print(e);
			});
		
	}
	Widget getTotal()
	{
		return Card(
			child: Container(
				decoration: BoxDecoration(
					 borderRadius: BorderRadius.circular(10),
					 color: Color(0xffFF4C58),
					 /*
					 border: new Border.all(color: Colors.grey, width:1),
					 boxShadow: [
						BoxShadow(
							color: Colors.grey,
							offset: Offset(2, 2),
							blurRadius: 4,
						)
					 ]
					 */
				),
				padding: EdgeInsets.all(10),
				child: Column(
					children: [
						Row(
							children:[
								Expanded(
									child: Text(
										'Total del pedido',
										style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
									)
								),
								Text(
									this._cart.getTotals().toStringAsFixed(2) + ' Bs.', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
								),
							]
						),
						SizedBox(height: 10),
						Row(
							children:[
								Expanded(
									child: Text(
										'Costo delivery',
										style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
									)
								),
								Text(
									this._cart.getDeliveryCost().toStringAsFixed(2) + ' Bs.', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
								),
							]
						),
						SizedBox(height: 10),
						Row(
							children:[
								Expanded(
									child: Text(
										'Total a pagar',
										style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
									)
								),
								Text(
									(this._cart.getTotals() + this._cart.getDeliveryCost()).toStringAsFixed(2)  + ' Bs.', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
								),
							]
						),
					]
				)
			),
		);
	}
	Widget getShipping()
	{
		
		//crossAxisAlignment: CrossAxisAlignment.stretch,
		return Card(
			child: Container(
				padding: EdgeInsets.all(10),
				child: Column(
					children: [
						Text('Datos para pedido', 
							style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)
						),
						SizedBox(height:10),
						FormGroup(
							label: 'Nombre',
							child: TextFormField(
								controller: this._ctrlName,
								validator: (v)
								{
									if( v.isEmpty )
										return 'Debe ingresar su nombre';
								}
							)
						),
						FormGroup(
							label: 'Email',
							child: TextFormField(
								keyboardType: TextInputType.emailAddress,
								controller: this._ctrlEmail,
								validator: (v)
								{
									if( v.isEmpty )
										return 'Debe ingresar su email';
								}
							)
						),
						FormGroup(
							label: 'Teléfono',
							child: TextFormField(
								keyboardType: TextInputType.phone,
								controller: this._ctrlPhone,
								validator: (v)
								{
									if( v.isEmpty )
										return 'Debe ingresar su número de teléfono';
								}
							)
						),
						/*
						FormGroup(
							label: 'Zona',
							child: Row(
								//crossAxisAlignment: CrossAxisAlignment.stretch, 
								children:[
									Expanded(
										child: DropdownButton(
											hint: Text('Seleccione su zona'),
											onChanged: (zid)
											{
												var barrios = [];
												this.barrioId = null;
												this._zonas.forEach((zona)
												{
													if( zona['id'].toString() == zid.toString() )
													{
														if( zona['barrios'] != null )
														{
															barrios.addAll(zona['barrios']);
														}
													}
												});
												this.setState(()
												{
													this._barrios = barrios;
													this.zonaId = zid.toString();
												});
												
											},
											value: this.zonaId,
											items: this._zonas.map( (zona)
											{
												return DropdownMenuItem<String>(
													value: zona['id'].toString(),
													child: Text(zona['nombre'])
												);
											}).toList()
										)
									)
								]
							)
						),
						FormGroup(
							label: 'Barrio',
							child: Row(
								//crossAxisAlignment: CrossAxisAlignment.stretch, 
								children:[
									Expanded(
										child: DropdownButton(
											hint: Text('Seleccione su barrio'),
											onChanged: (bid)
											{
												this.setState(()
												{
													this.barrioId = bid.toString();
												});
												
											},
											value: this.barrioId,
											items: this._barrios.map( (barrio)
											{
												return DropdownMenuItem<String>(
													value: barrio['id'].toString(),
													child: Text(barrio['nombre'])
												);
											}).toList()
										)
									)
								]
							)
						),
						*/
						FormGroup(
							label: 'Dirección',
							child: TextFormField(
								//style: Theme.of(context).textTheme.body1,
								keyboardType: TextInputType.text,
								controller: this._ctrlAddress,
								validator: (v)
								{
									if( v.isEmpty )
										return 'Debe ingresar la direccion para el envio';
								}
							)
						),
						if( this._enableShippingDate )
							FormGroup(
								label: 'Fecha para la entrega',
								child: DateTimeField(
									validator: (val)
									{
										if( val == null )
											return 'Debe seleccionar la fecha y hora para la entrega';
									},
									controller: this._ctrlDateTime,
									format: this.dateFormat,
									onShowPicker: (ctx, currentValue) async
									{
										final date = await showDatePicker(
											context: ctx,
											firstDate: DateTime(1900),
											initialDate: currentValue ?? DateTime.now(),
											lastDate: DateTime(2100),
										);
										if( date != null )
										{
											final time = await showTimePicker(context: ctx, initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()));
											return DateTimeField.combine(date, time);
										}
										else
											return currentValue;
									}
								)
							),
						FormGroup(
							label: 'Método de Pago',
							child: TextFormField(
								readOnly: true,
								controller: this._ctrlPaymentMethod,
								validator: (val)
								{
									if( val.isEmpty )
										return 'Debe seleccionar el metodo de pago';
								},
								onTap: ()
								{
									this._showPaymentMethods();
								},
							)
						)
					]
				)
			)
		);
	}
	int	_radioZone = -1;
	int _radioMethod = -1;
	Widget getShippingFields()
	{
		var card = Card(
			child: Container(
				padding: EdgeInsets.all(8),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Text('Datos para el envio', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
						SizedBox(height: 8),
						for(WCShippingZone z in this._zones)
							Row(
								children: [
									Expanded(child: Text(z.name)),
									SizedBox(width: 10),
									Radio(
										value: z.id,
										groupValue: this._radioZone,
										onChanged: (int zid)
										{
											this.setState(()
											{
												this._radioZone = zid;
											});
											this._loadShippingMethods(zid);
										},
									)
								]
							)
					]
				)
			),
		);
		Widget cardM = SizedBox(height: 0);
		if( this._shippingMethods.length > 0 )
		{
			cardM = Card(
				child: Container(
					padding: EdgeInsets.all(8),
					child: Column(
						children: [
							for(WCShippingMethod m in this._shippingMethods)
								Column(
									children: [
										Row(
											children: [
												Expanded(child: Text(m.title + ' (' + m.cost.toStringAsFixed(2) + ' Bs)')),
												SizedBox(width: 10),
												Radio(
													value: m.id,
													groupValue: this._radioMethod,
													onChanged: (int rid)
													{
														this.setState(()
														{
															this._radioMethod = rid;
															this._cart.deliveryFee = m.cost;
														});
													},
												)
											],
										),
										SizedBox(height: 8),
									]
								)
						]
					)
				)
			);
		}
		return Column(
			children: [
				card,
				cardM	
			]
		);
	}
	void _loadShippingMethods(int zoneId) async
	{
		this._radioMethod = -1;
		var methods = await WooService.getShippingMethods(zoneId);
		this.setState(()
		{
			this._shippingMethods = methods;
		});
	}
	void _showPaymentMethods()
	{
		showModalBottomSheet(context: context, builder: (ctx) 
		{
			return SafeArea(
				child: StatefulBuilder(
					builder: (_ctx, setter)
					{
						return Container(
							//height: 520,
							color: Color(0xFF737373),
							child: Container(
								//height: 450,
								padding: EdgeInsets.all(10),
								decoration: BoxDecoration(
									color: Theme.of(context).canvasColor,
									borderRadius: BorderRadius.only(
										topLeft: Radius.circular(10),
										topRight: Radius.circular(10),
									),
								),
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									children: [
										Text('Seleccionar tu método de pago para tu pedido', textAlign: TextAlign.center, style: TextStyle(fontSize: 18, fontWeight:FontWeight.bold)),
										SizedBox(height:15),
										Text('Selecciona tu método preferido de pago para tu pedido, esta información nos ayuda a proporcionarte un servicio más rápido y adecuado.'),
										SizedBox(height: 8),
										Expanded(
											child: ListView(
												children: [
													ListTile(
														leading: Icon(Icons.credit_card, color: Color(0xff0065ab),),
														title: Text('Tarjeta de Crédito/Débito'),
														onTap: ()
														{
															this._paymentMethod = 'lckout';
															this.setState(()
															{
																this._ctrlPaymentMethod.text = 'Tarjeta de Crédito/Débito';
															});
															Navigator.pop(context);
														},
													),
													ListTile(
														leading: FaIcon(FontAwesomeIcons.fileInvoiceDollar, color: Color(0xff0065ab),), //Icon(Icons.settings_system_daydream),
														title: Text('Tranferencia Bancaria'),
														onTap: ()
														{
															this._paymentMethod = 'wire';
															this.setState(()
															{
																this._ctrlPaymentMethod.text = 'Tranferencia Bancaria';
															});
															Navigator.pop(context);
														}
													),
													ListTile(
														leading: FaIcon(FontAwesomeIcons.university, color: Color(0xff0065ab),),//Icon(Icons.money_off),
														title: Text('Deposito en Cuenta'),
														onTap: ()
														{
															this._paymentMethod = 'bank_account';
															this.setState(()
															{
																this._ctrlPaymentMethod.text = 'Depósito en Cuenta';
															});
															Navigator.pop(context);
														}
													),
													ListTile(
														leading: FaIcon(FontAwesomeIcons.moneyBill, color: Color(0xff0065ab),),//Icon(Icons.attach_money),
														title: Text('Efectivo al momento de la entrega'),
														onTap: ()
														{
															this._paymentMethod = 'cash';
															this.setState(()
															{
																this._ctrlPaymentMethod.text = 'Efectivo al momento de la entrega';
															});
															Navigator.pop(context);
														}
													),
												]
											)
										)										
									]
								)
							)
						);
					},
				)
			);
		});
	}
	Widget _getButtons()
	{
		return Container(
			color: Colors.white,
			padding: EdgeInsets.all(8),
			child: Row(
				children: [
					Expanded(
						child: WidgetButton(
							color: Color(0xffd8d4d0),
							text: 'Seguir Comprando', 
							callback: () => this._continueShopping()
						)
					),
					SizedBox(width:10),
					Expanded(
						child: WidgetButton(
							color: Color(0xfff2b2b2),
							text: 'Enviar pedido', 
							callback: () => this._sendOrder(context)
						)
					),
					/*
					SizedBox(width:10),
					Expanded(
						child: WidgetButton(
							text: 'Pedido Express', type: 'secondary', 
							icon: FaIcon(FontAwesomeIcons.whatsapp), 
							callback: ()
							{
								this._sendExpressOrder();
							}
						)
					),
					*/
				]
			)
		);
	}
	@override
	void initState()
	{
		super.initState();
		this._ctrlName 		= TextEditingController();
		this._ctrlEmail 	= TextEditingController();
		this._ctrlPhone 	= TextEditingController();
		this._ctrlAddress 	= TextEditingController();
		this._ctrlDateTime	= TextEditingController();
		ServiceUsers.getCurrentUser()
			.then( (user) 
			{
				if( user != null )
				{
					this.user = user;
					this.setState(()
					{
						this._ctrlName.text = user.fullname;
						this._ctrlEmail.text = user.email;
						/*
						if( this.user.meta['_phone'] != null )
							this._ctrlPhone.text = this.user.meta['_phone'];
						*/
					});
					
				}
			})
			.catchError( (e) 
			{
				print(e);
			});
		SB_Settings.getString('location')
			.then( (d)  
			{
				this.gdata = json.decode(d);
				
				SB_Geo.getAddress(this.gdata['lat'], this.gdata['lng'])
					.then( (address) 
					{
						this.setState(()
						{
							this._ctrlAddress.text = address;
						});
					});
				
			})
			.catchError( (e) 
			{
				print('ERROR GETTING LOCATION');
			});
		WooService.getShippingZones().then( (zones) 
		{
			this.setState(()
			{
				this._zones = zones;
			});
		});
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: Text('Información para el pedido'),
			),
			body: SafeArea(
				child: Column(
					children: [
						this.getTotal(),
						SizedBox(height:10),
						Expanded(
							child: ListView(
								children: [
									this.getShipping(),
									SizedBox(height:20),
									this.getShippingFields(),
								]
							)
						),
						this._getButtons(),
					]
				)
			),
			/*
			body: SafeArea(
				child: SingleChildScrollView(
					child: Container(
						margin: EdgeInsets.all(10),
						child: Form(
							key: this._formKey,
							child: Column(
								children: [
									this.getTotal(),
									SizedBox(height:10),
									this.getShipping(),
									SizedBox(height:20),
									this.getShippingFields(),
									this._getButtons()
								]
							)
						)
					)
				)
			)
			*/
		);
	}
	void _continueShopping()
	{
		
	}
	void _sendExpressOrder() async
	{
		var settings = SB_Globals.getVar('settings');
		//if( settings != null && settings['whatsapp_order_number'] != null && this._formKey.currentState.validate() )
		{
			this._buildOrder();
			String url = 'https://web.whatsapp.com/send?phone=59177218666&text=Hello text';
			print(url);
			if( await canLaunch(url) )
			{
				print(url);
				await launch(url, forceSafariVC: false, forceWebView: false);
			}
		}
	}
}
