import 'package:flutter/material.dart';
import '../../Classes/SB_Cart.dart';
import 'WidgetCartItem.dart';
import '../WidgetButton.dart';
import 'WidgetCheckout.dart';
import '../../Services/ServiceUsers.dart';
import '../Users/WidgetLogin.dart';
import '../../Classes/SB_Globals.dart';

class WidgetCart extends StatefulWidget
{
	@override
	WidgetCartState		createState() => WidgetCartState();
}
class WidgetCartState extends State<WidgetCart> with RouteAware
{
	SB_Cart	_cart 	= SB_Cart();
	bool editing	= false;
	List<Widget>	cartItems = [];
	
	void _updateCart()
	{
		this.setState(()
		{
			this._cart = SB_Cart();
		});
	}
	List<Widget> _buildItems()
	{
		double delivery_amount = this._cart.getDeliveryCost();
		var items = <Widget>[];
		this._cart.getItems().forEach( (cartItem) 
		{
			items.add(
				WidgetCartItem(item: cartItem, editing: this.editing, notifyCart: this._updateCart)
			);
		});
		
		if( items.length <= 0 )
		{
			items.add( Container(
				margin: EdgeInsets.all(10),
				child: Center(
					child: Text(
						'No existen productos en su pedido', 
						textAlign: TextAlign.center,
						style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold) 
					) 
				)
			));
		}
		else if( !this.editing )
		{
			//##delivery fee
			items.add(
				Container(
					margin: EdgeInsets.all(10),
					child: Row(
						children: [
							Expanded(
								child: Container(
									margin: EdgeInsets.only(right: 15),
									child: Text(
										'Costo delivery', 
										style: TextStyle(
											fontWeight: FontWeight.bold,
										),
										textAlign: TextAlign.right
									)
								)
							),
							Text(delivery_amount.toStringAsFixed(2), style: TextStyle(fontWeight: FontWeight.bold) )
						]
					)
				)
			);
			//##add totals
			items.add(
				Container(
					margin: EdgeInsets.all(10),
					child: Row(
						children: [
							Expanded(
								child: Container(
									margin: EdgeInsets.only(right: 15),
									child: Text(
										'Totals', 
										style: TextStyle(
											fontWeight: FontWeight.bold,
										),
										textAlign: TextAlign.right
									)
								)
							),
							Text(this._cart.getTotals().toStringAsFixed(2), style: TextStyle(fontWeight: FontWeight.bold) )
						]
					)
				)
			);
		}
		return items;
	}
	@override
	Widget build(BuildContext context)
	{
		var actions = <Widget>[];
		this.setState( () 
		{
			this.cartItems = this._buildItems();
			if( !this.editing )
			{
				actions.addAll([
					IconButton(
						tooltip: 'Modificar mi pedido',
						icon: Icon(Icons.edit),
						onPressed: ()
						{
							print('Editando pedido');
							this.setState(() 
							{
								this.editing = true;
							});
						}
					),
				]);
			}
			else
			{
				actions.addAll([
					IconButton(
						tooltip: 'Guardar cambios en el pedido',
						icon: Icon(Icons.save),
						onPressed: ()
						{
							this.setState(() 
							{
								this.editing = false;
							});
						}
					),
				]);
			}
		});
		return Scaffold(
			appBar: AppBar(
				title: Text('Detalles de su pedido'),
				actions: actions
			),
			body: SafeArea(
				child: Column(
					children: <Widget>[
						Expanded(child: ListView(children: this.cartItems)),
						Row(
							children: (this._cart.getItems().length > 0) ? [
								Expanded(
									child: Container(
										margin: EdgeInsets.all(8), 
										child: WidgetButton(text: 'Seguir comprando', callback: null, color: Color(0xffd8d4d0),)
									)
								),
								Expanded(
									child: Container(
										margin: EdgeInsets.all(8),
										child: WidgetButton(
											text: 'Procesar pedido', 
											callback: ()
											{
												ServiceUsers.isLoggedIn().then( (bool authenticated) 
												{
													Navigator.push(context, MaterialPageRoute(
														builder: (context) => authenticated ? WidgetCheckout() : WidgetLogin()
													));
												});
												
											}, 
											color: Color(0xfff2b2b2), //Color(0xffd8d4d0),
											//type: 'secondary'
										) 
									)
								)
							] : []
						)
					]
				)
			)
		);
	}
	
}
