import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WidgetPayment extends StatelessWidget
{
	final	int	orderId;
	
	WidgetPayment({this.orderId});
	
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Proceso de pago online')),
			body: Container(
				child: WebView(
					userAgent: 'Fradas Wissco User Agent/1.0.0',
					javascriptMode: JavascriptMode.unrestricted,
					javascriptChannels: [
						JavascriptChannel(
							name: 'payment_success',
							onMessageReceived: (JavascriptMessage msg)
							{
								msg.message;
								Navigator.pop(context);
							}
						),
						JavascriptChannel(
							name: 'payment_cancel',
							onMessageReceived: (JavascriptMessage msg)
							{
								Navigator.pop(context);
							}
						)
					].toSet(),
					initialUrl: 'https://fradaswissco.com/?view=web_payment&oid=${this.orderId}'
				)
			)
		);
	}
}