import 'package:flutter/material.dart';
import '../WidgetButton.dart';
import '../Users/WidgetOrders.dart';
import '../../Classes/WCOrder.dart';
import '../../main.dart';
import 'WidgetPayment.dart';

class WidgetOrderSuccess extends StatelessWidget
{
	final WCOrder	order;
	
	WidgetOrderSuccess({this.order});
	
	@override
	Widget build(BuildContext context)
	{
		var sf = Scaffold(
			body: SafeArea(
				child: Container(
					margin: EdgeInsets.only(right: 10, left: 10),
					child: Column(
						children: [
							SizedBox(height:50),
							Center(
								/*
								child: Container(
									width: 80,
									height: 80,
									decoration: BoxDecoration(
										color: Colors.green,
										borderRadius: BorderRadius.circular(50)
									),
									child: Icon(
										Icons.check,
										color: Colors.white,
										size: 50,
									),
								)
								*/
								child: Image.asset('images/woman-01.png', fit: BoxFit.cover)
							),
							SizedBox(height:50),
							Center(
								child: Text('Su pedido fue recibido correctamente!!!', 
									textAlign: TextAlign.center,
									style: TextStyle(fontSize: 30, )
								)
							),
							SizedBox(height:50),
							Center(
								child: Text(
									'Nos pondremos en contacto contigo en un lapso de 24 horas para confirmar tu pedido.', 
									style: TextStyle(fontSize: 15),
									textAlign: TextAlign.center,
								)
							),
							SizedBox(height:50),
							WidgetButton(
								color: Color(0xffFF4C58),
								text: 'Seguir comprando', 
								callback: ()
								{
									Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => MyHomePage()), (_) => false);
								}, 
								type: 'primary'
							),
							SizedBox(height:50),
							WidgetButton(
								text: 'Ver mis pedidos', 
								callback: ()
								{
									//Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (ctx) => WidgetOrders()), (_) => false);
									Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetOrders()));
								}, 
								type: 'warning'
							),
						]	
					)
				)
			)
		);
		
		return LayoutBuilder(
			builder: (ctx, constraint)
			{
				WidgetsBinding.instance.addPostFrameCallback((_) 
				{
					print('Payment method: ${this.order.payment_method}');
					print('Payment method: ${this.order.payment_method_title}');
					if( this.order.payment_method == 'lckout' )
					{
						Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetPayment(orderId: this.order.id)));
					}
				});
				return sf;
			}
		);
	}
}
