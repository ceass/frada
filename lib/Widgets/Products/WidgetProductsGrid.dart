import 'package:flutter/material.dart';
import '../../Classes/WCProduct.dart';
import '../../Services/WooService.dart';
import 'WidgetProductGridItem.dart';
//import '../../Classes/MB_Category.dart';

class WidgetProductsGrid extends StatefulWidget
{
	final	category;
	
	WidgetProductsGrid({this.category});
	@override
	WidgetProductsGridState	createState() => WidgetProductsGridState();
}
class WidgetProductsGridState extends State<WidgetProductsGrid>
{
	ScrollController 	_controller;
	int					currentPage = 0;
	List<WCProduct>	childs = [];
	Future<List>		loader;
	bool				isLoading = false;
	
	void _loadMore()
	{
		if( this.isLoading )
			return;
		this.isLoading = true;
		int nextPage = this.currentPage + 1;
		print('Loading page: ${nextPage}');
		this.setState(()
		{
			/*
			this.loader = this.widget.category != null ? 
						ServiceProducts.getCategoryProducts(this.widget.category.category_id, 8, nextPage) : 
						ServiceProducts.getProducts();
			*/
			
		});	
		if( this.widget.category != null )
		{
			/*
			WooService.getCategoryProducts(this.widget.category.category_id, 8, nextPage)
				.then( (items) 
				{
					if( items.length > 0 )
					{
						this.setState( () 
						{
							this.childs.addAll((items as List<MB_Product>));
						});
						this.currentPage++;
					}
					this.isLoading = false;
					
				});
			*/
		}
		else
		{
			WooService.getProducts().then( (items) 
			{
				if( items.length > 0 )
				{
					this.setState( () 
					{
						this.childs.addAll((items as List<WCProduct>));
					});
					this.currentPage++;
				}
				this.isLoading = false;
				
			});
		}			
		
	}
	void _scrollListener()
	{
		if (this._controller.offset >= this._controller.position.maxScrollExtent && !this._controller.position.outOfRange) 
		{
			print('reach the bottom');
			this._loadMore();
			/*
			this.setState(() {
		    	//message = "reach the bottom";
		 	});
			*/
		}
		if (this._controller.offset <= this._controller.position.minScrollExtent && !this._controller.position.outOfRange) 
		{
			/*
			this.setState(() {
		    	//message = "reach the top";
			});
			*/
		}
	}
	@override
	void initState()
	{
		this._controller = ScrollController();
		this._controller.addListener(this._scrollListener);
		super.initState();
		this._loadMore();
	}
	@override
	Widget build(BuildContext context)
	{
		var mediaQuery = MediaQuery.of(context);
		var size = mediaQuery.size;
	    /*24 is for notification bar on Android*/
	    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
	    final double itemWidth = size.width / 2;

		//MediaQuery.of(context).orientation  == Orientation.portrait ? 2 : 3
		int lineCount = 2;
		if( size.width > 600 )
			lineCount = 4;
		
		return GridView.builder(
			//shrinkWrap: true,
			//primary: false,
			//childAspectReadio: (itemWidth / itemHeight),
			itemCount: this.childs.length,
			gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
				crossAxisCount: lineCount,
				childAspectRatio: 0.8 //mediaQuery.size.width / (mediaQuery.size.height / 1),
			),
			controller: this._controller,
			itemBuilder: (context, index)
			{
				return WidgetProductGridItem(product: this.childs[index]);
			}
		);
		
		/*
		return FutureBuilder<List>(
			future: this.loader,
			builder: (context, snapshot)
			{
				if( snapshot.hasData )
				{
					
				}
				else if( snapshot.hasError )
				{
					return Text('Ocurrio un error al recuperar los productos');
				}
				
				return Center(child:CircularProgressIndicator());
			}
		);
		*/
	}
}
