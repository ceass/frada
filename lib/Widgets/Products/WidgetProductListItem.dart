import 'package:flutter/material.dart';
import '../../Classes/WCProduct.dart';
import '../../Services/WooService.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'WidgetSingleProduct.dart';

class WidgetProductListItem extends StatelessWidget
{
	WCProduct	product;
	
	WidgetProductListItem({this.product});
	
	@override
	Widget build(BuildContext context)
	{
		var container = Card(
			//padding: EdgeInsets.all(8),
			//height: 300,
			child: Container(
				
				decoration: BoxDecoration(
					//border: Border(bottom: BorderSide(width: 1, color: Colors.black38)),
					//borderRadius: const BorderRadius.all(const Radius.circular(8))
					color: Colors.white
				),
				padding: EdgeInsets.all(8),
				//margin: EdgeInsets.only(bottom: 10),
				child: Row(
					children:[
						Image.network(
							this.product.getThumbnailUrl(), 
							width: 50, height: 50, 
							fit: BoxFit.fill
						),
						Expanded(
							child: Container(
								margin: EdgeInsets.only(right: 8, left: 8),
								child: Center( 
									child: Text(
										this.product.name, 
										textAlign: TextAlign.center, 
										//style: TextStyle(fontSize:17)
									) 
								)
							)
						),
						Container( 
							child: Text(
								this.product.getPrice(), 
								textAlign: TextAlign.center,
								style: TextStyle(fontWeight: FontWeight.bold)
							),
						)
					]
					
				)
			),
		);
		return InkWell(
			child: container, 
			onTap: () 
			{
				Navigator.push(context, MaterialPageRoute(
					builder: (context) => WidgetSingleProduct(product: this.product)
				));
			}
		);
	}
}
