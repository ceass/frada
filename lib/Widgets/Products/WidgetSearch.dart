import 'package:flutter/material.dart';
import 'dart:async';
import '../../Services/WooService.dart';
import '../../Classes/WCProduct.dart';
import 'WidgetProductListItem.dart';
import '../../Classes/SB_Globals.dart';

class WidgetSearch extends StatefulWidget
{
	@override
	_WidgetSearchState createState() => _WidgetSearchState();
}
class _WidgetSearchState extends State<WidgetSearch> with RouteAware
{
	TextEditingController	_searchController;
	var						_timer;
	List<WCProduct>			_results;
	Widget					_widgetResult;
	
	@override
	void initState()
	{
		super.initState();
		this._searchController = TextEditingController();
		this._results = List();
		this._widgetResult = Text(' ');
	}
	void _setProcessing()
	{
		this.setState(()
		{
			this._widgetResult = Column(
				children: [
					Text('Buscando...'),
					SizedBox(height: 8),
					CircularProgressIndicator()
				]
			);
		});
		
	}
	void _keywordChanged(String value)
	{
		print(value);
		if( this._timer != null )
			this._timer.cancel();
		this._setProcessing();
		this._timer = Timer(Duration(seconds: 1), ()
		{
			print('Searching => ' + value);
			WooService.search(value)
				.then( (data) 
				{
					this.setState( () 
					{
						this._results = data;
						this._widgetResult = ListView.builder(
							itemCount: this._results.length,
							itemBuilder: (BuildContext ctx, int index)
							{
								return WidgetProductListItem(product: this._results[index]);
							}
						);
					});
					
				})
				.catchError( (error)
				{
					print('ERROR');
					print(error);
				});
		});	
		
		/*Timer.periodic(Duration(seconds: 2), (timer)
		{
			print("Pausing Isolate 1");
			//isolate.pause();
		});*/
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: Text('Búsqueda de productos'),
				actions: [
					
				]
			),
			body: Container(
				margin: EdgeInsets.all(10),
				child: Column(
					children: [
						Row(
							children:[
								Expanded(
									child: TextField(
										decoration: InputDecoration(
											hintText: 'Escriba el nombre del producto',
										),
										style: TextStyle(color: Colors(0xffffffff)),
										controller: this._searchController,
										onChanged: this._keywordChanged,
									)
								)
							]
						),
						Container(
							margin: EdgeInsets.all(10),
							child: Text(
								'Resultados de Busqueda', 
								style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)
							)
						),
						Expanded(
							child: this._widgetResult
						),
						
					]
				)
			)
		);
	}
	@override
	void didChangeDependencies()
	{
		super.didChangeDependencies();
		print('WidgetSearch -> didChangeDependencies');
		if( SB_Globals.getVar('floating_avatar') != null )
		{
			(SB_Globals.getVar('floating_avatar') as OverlayEntry).remove();
			SB_Globals.setVar('floating_avatar', null);
		}
	}
	@override
	void dispose()
	{
		super.dispose();
	}
	@override
	void didPush()
	{
		print('Search pushed (didPush)');
	}
}
