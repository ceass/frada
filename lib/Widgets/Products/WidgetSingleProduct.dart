import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:share/share.dart';
import '../../Classes/WCProduct.dart';
import '../WidgetNumberPicker.dart';
import '../WidgetBottomMenu.dart';
import '../../Classes/SB_Cart.dart';
import '../Cart/WidgetCart.dart';

class WidgetSingleProduct extends StatefulWidget
{
	final WCProduct	product;
	
	WidgetSingleProduct({this.product});
	@override
	_WidgetSingleProductState	createState() => _WidgetSingleProductState();
}
class _WidgetSingleProductState extends State<WidgetSingleProduct>
{
	SB_Cart		_cart = SB_Cart();
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			body: Container(
				color: Colors.white,
				child: CustomScrollView(
					slivers: <Widget>[
						SliverAppBar(
							floating: true,
							title: Text(this.widget.product.name),
							backgroundColor: Colors.transparent,
							expandedHeight: 300,
							flexibleSpace: FlexibleSpaceBar(
								background: FittedBox(
									fit: BoxFit.fill,
									child: Image.network(this.widget.product.getThumbnailUrl(), fit: BoxFit.contain,)
								)
							)
						),
						SliverList(
						//SliverFixedExtentList(
							//itemExtent: 300,
							delegate: SliverChildListDelegate([
								Container(
									
									padding: EdgeInsets.only(top: 8, right: 12, bottom: 9, left: 12),
									child: Column(
										children: [
											Row(
												children:[
													Expanded(
														child: Text(this.widget.product.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
													),
													Row(
														children: [
															IconButton(
																icon: Icon(Icons.share,),
																color: Color(0xffFF4C58),
																onPressed: ()
																{
																	Share.share(this.widget.product.name + ' ' + this.widget.product.permalink);
																},
															),
															SizedBox(width: 8),
															IconButton(
																icon: FaIcon(FontAwesomeIcons.solidHeart),
																color: Color(0xff8D8D8D),
																onPressed: ()
																{
																	var alert = AlertDialog(
																		title: Text("Producto adicionado a favoritos"),
																		content: Text("Su producto fue adicionado a sus favoritos."),
																		actions: <Widget>[
																			FlatButton(
																				child: Text("Cerrar"), 
																				onPressed: ()
																				{
																					Navigator.of(context).pop();
																				}
																			)
																		]
																	);
																	showDialog(context: context, builder: (BuildContext ctx){return alert;});
																}
															)
														]
													)
												]
											),
											Row(
												children:[
													Expanded(
														child: Column(
															crossAxisAlignment: CrossAxisAlignment.stretch,
															children: [
																Text(this.widget.product.getPrice(), style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))
															]
														)
													),
													Expanded(
														child: Container(
															child: WidgetNumberPicker(
																btnColor: Color(0xffFF4C58),
																onQuantityChanged: (qty)
																{
																	var ci = this._cart.productExists(this.widget.product.id);
																	this.setState(()
																	{
																		if( ci != null )
																		{
																			ci.setQuantity(qty);
																		}
																		else
																		{
																			this._cart.addProduct(this.widget.product, qty);	
																		}
																	});
																}
															),
														)
													)
												]
											),
											Html(
												data: this.widget.product.description,
												style: {
													"div": Style(
														color: Color(0xff8D8D8D)
													)
												}
											)
										]
									)
								)
							]),
						)
					],
				)
			),
			floatingActionButton: this._cart.getItems().length > 0 ? FloatingActionButton(
				child: Icon(Icons.shopping_cart),
				backgroundColor: Color(0xffFF4C58),
				foregroundColor: Colors.white,
				onPressed: ()
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetCart()));
				},
			) : null,
			bottomNavigationBar: WidgetBottomMenu(currentIndex: 1,),
		);
	}
}