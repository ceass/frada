import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import 'package:provider/provider.dart';
import '../../Classes/WCProduct.dart';
import 'WidgetSingleProduct.dart';
import '../../Classes/SB_Cart.dart';
import '../../Providers/CartProvider.dart';

class WidgetProductGridItem extends StatelessWidget
{
	WCProduct	product;
	SB_Cart		_cart = SB_Cart();
	
	WidgetProductGridItem({this.product});
	
	void _addToCart(context)
	{
		this._cart.addProduct(this.product);
		Provider.of<CartProvider>(context, listen: false).quantity = this._cart.getItems().length;
	}
	@override
	Widget build(BuildContext context)
	{
		var mediaQuery = MediaQuery.of(context);
		double imgHeight = 140;
		//MediaQuery.of(context).orientation  == Orientation.portrait ? 130 : 230;
		//if( mediaQuery.size.width > 600 )
		//	imgHeight = 200;
		var container = Container(
			//color: Colors.white,
			decoration: BoxDecoration(
				border: Border.all(width: 1, color: Colors.grey[200]),
				borderRadius: const BorderRadius.all(const Radius.circular(8)),
				color: Colors.white,
			),
			margin: EdgeInsets.all(7),
			child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Row(
							children: [
								Expanded(
									child: Container(
										padding: EdgeInsets.only(left: 8),
										child: Text(this.product.name, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))
									) 
								),
								Row(
									children: [
										IconButton(
											icon: Icon(Icons.share),
											color: Color(0xffFF4C58),
											onPressed: ()
											{
												Share.share(this.product.name + ' ' + this.product.permalink);
											},
										),
										SizedBox(width: 8),
										IconButton(
											icon: FaIcon(FontAwesomeIcons.shoppingBag),
											color: Color(0xffFF4C58),
											onPressed: ()
											{
												this._addToCart(context);
											}
										)
									]
								)
							]
						),
						SizedBox(height: 10),
						InkWell(
							child: Container(
								color: Color(0xfff0f0ed),
								child: Row(
									children:[
										Flexible(
											flex: 1,
											child: Container(
												child: Image.network(
													this.product.getThumbnailUrl(), 
													//height: imgHeight, 
													fit: BoxFit.cover
												)
											),
										),
										Flexible(
											flex: 1,
											fit: FlexFit.tight,
											child: Container(
												padding: EdgeInsets.all(8),
												child: Column(
													crossAxisAlignment: CrossAxisAlignment.stretch,
													//mainAxisSize: MainAxisSize.max,
													mainAxisAlignment: MainAxisAlignment.start,
													children: [
														Text(this.product.getExcerpt()),
														Center(
															child: Text(this.product.getPrice(), style: TextStyle(fontSize: 22))
														)
													]
												)
											)
										)
									]
								),
							),
							onTap: ()
							{
								Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetSingleProduct(product: this.product)));
							},
						),
					]
			)
		);
		
		return container;
	}
}
