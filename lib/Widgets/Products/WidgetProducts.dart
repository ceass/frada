import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'WidgetProductsGrid.dart';
import '../../Services/WooService.dart';
import 'WidgetProductGridItem.dart';
import '../../Classes/WCProduct.dart';
import '../../Classes/WCCategory.dart';
import '../WidgetBottomMenu.dart';
import 'WidgetSearch.dart';
import '../../Classes/SB_Cart.dart';
import '../Cart/WidgetCart.dart';
import '../../Providers/CartProvider.dart';

class WidgetProducts extends StatefulWidget
{
	@override
	WidgetProductsState	createState() => WidgetProductsState();
}
class WidgetProductsState extends State<WidgetProducts>
{
	List<WCProduct>		_products = [];
	List<WCCategory>	_categories = [];
	Widget				_productsWidget;
	ScrollController	_scrollController	= ScrollController();
	int					_currentPage = 0;
	WCCategory			_currentCat;
	SB_Cart				_cart = SB_Cart();
	bool				_loading = false;
	
	void _scrollListener()
	{
		if( this._scrollController.offset >= this._scrollController.position.maxScrollExtent && !this._scrollController.position.outOfRange )
		{
			if( this._currentCat != null )
			{
				print('Scroll bottom, we need to load more products... ');
				WooService.getProducts(this._currentPage + 1, 10, this._currentCat.id)
					.then((prods)
					{
						print('Products loaded: ${prods.length}');
						if( prods.length > 0 )
						{
							this._currentPage++;
							this._setProducts(prods);
						}
					})
					.catchError((e)
					{
						print('Error loading more products');
						print(e);
					});
			}
			
		}	
	}
	Widget _getLoadingProducts()
	{
		return Column(
			mainAxisAlignment: MainAxisAlignment.center,
			children: [
				Text('Obteniendo productos...'),
				SizedBox(height: 10),
				CircularProgressIndicator()
			]
		);
	}
	void _setLoadingProducts()
	{
		this.setState(()
		{
			this._productsWidget = this._getLoadingProducts();
		});
	}
	Widget _getProductsWidget()
	{
		return ListView.builder(
			controller: this._scrollController,
			itemCount: this._products.length,
			itemBuilder: (_ctx, index)
			{
				var prod = this._products[index];
				return  WidgetProductGridItem(product: prod,);
			},
		);
	}
	void _setProductsWidget([List prods = null])
	{
		if( prods != null )
			this._products = prods;
			
		var listView = this._getProductsWidget();
		this.setState(()
		{
			this._productsWidget = listView;
		});
	}
	void _setProducts(products)
	{
		this.setState(()
		{
			//this._products += products;
			//this._productsWidget = this._productsWidget;
			this._products.addAll(products); 
		});
	}
	void _getCategoryProducts(WCCategory cat)
	{
		//this._setLoadingProducts();
		this.setState(()
		{
			this._loading = true;
		});
		this._currentPage	= 1;
		this._currentCat	= cat;
		
		WooService.getProducts(this._currentPage, 10, cat.id)
			.then((prods)
			{
				this.setState(()
				{
					this._loading = false;
					this._products = prods;
				});
				//this._setProductsWidget(prods);
			});
	}
	@override
	void initState()
	{
		super.initState();
		this._scrollController.addListener(this._scrollListener);
		this._setLoadingProducts();
		WooService.getCategories().then((cats)
		{
			this.setState(()
			{
				this._categories = cats;
			});
		});
		this._loading = true;
		WooService.getProducts().then((products)
		{
			this.setState(()
			{
				this._loading = false;
				this._products = products;
			});
			
			//this._setProductsWidget(products);
			//this._setProducts(products);
		});
	}
	@override
	Widget build(BuildContext context)
	{
		var cartProv = Provider.of<CartProvider>(context/*, listen: false*/); //.quantity = this._cart.getItems().length;
		return Scaffold(
			appBar: AppBar(
				title: Text('Nuestros Productos'),
				actions: <Widget>[
					IconButton(
						icon: Icon(Icons.search),
						onPressed: ()
						{
							Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetSearch()));
						}
					)
				],
			),
			bottomNavigationBar: WidgetBottomMenu(currentIndex: 1),
			floatingActionButton: cartProv.quantity > 0 ? FloatingActionButton(
				child: Icon(Icons.shopping_cart),
				backgroundColor: Color(0xffFF4C58),
				foregroundColor: Colors.white,
				onPressed: ()
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetCart()));
				},
			) : null,
			body: SafeArea(
				child: Container(
					/*
					child: CustomScrollView(
						slivers: [
							SliverAppBar(
								title: Text('Productos'),
								backgroundColor: Colors.transparent,
								expandedHeight: 200,
								flexibleSpace: FlexibleSpaceBar(
									background: Image.asset('images/products-bg.jpg', fit: BoxFit.cover,)
								)
							),
							SliverGrid(
								gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1, childAspectRatio: 1.45),
								delegate: SliverChildBuilderDelegate((ctx, index)
								{
									return WidgetProductGridItem(product: this._products[index],);
								}, childCount: this._products.length)						
							),
							
						]
					),
					*/
					child: Column(
						children: [
							SizedBox(height: 10),
							Container(
								height: 40,
								//color: Colors.red,
								child: ListView.builder(
									scrollDirection: Axis.horizontal,
									itemCount: this._categories.length,
									itemBuilder: (_ctx, index)
									{
										var cat = (this._categories[index] as WCCategory);
										return FlatButton(
											padding: EdgeInsets.all(0),
											child: Container(
												padding: EdgeInsets.all(7),
												margin: EdgeInsets.only(right: 8, left: 8),
												decoration: BoxDecoration(
													color: Colors.black,
													borderRadius: BorderRadius.circular(12),
												),
												child: Text(cat.name, style: TextStyle(color: Colors.white))
											),
											onPressed: ()
											{
												this._getCategoryProducts(cat);
											},
										);
									},
								),
							),
							SizedBox(height: 10),
							Container(
								child: Text(
									this._currentCat != null ? this._currentCat.name : 'Últimos Productos', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)
								)
							),
							SizedBox(height: 10),
							Expanded(
								child: this._loading ? this._getLoadingProducts() : this._getProductsWidget() //this._productsWidget
							)
						]
					)
				),
			)
			
		);
	}
}