import 'package:flutter/material.dart';
//import '../Services/WCServiceUsers.dart';
import '../Classes/SB_Settings.dart';
import '../main.dart';
//import 'WidgetMap.dart';
import 'Users/WidgetAccount.dart';
import 'Products/WidgetProducts.dart';
import 'Services/WidgetServices.dart';
import '../Services/ServiceUsers.dart';
import 'Users/WidgetLogin.dart';
import 'Cart/WidgetCart.dart';
import 'Users/WidgetAppointments.dart';
import 'Users/WidgetOrders.dart';
import 'WidgetMyLocation.dart';

class WidgetSideDrawer extends  StatefulWidget
{
	@override
	WidgetSideDrawerState	createState() => WidgetSideDrawerState();
}
class WidgetSideDrawerState extends State<WidgetSideDrawer>
{
	List<Widget>		_items = [];
	Function			t;
	
	void logout(context)
	{
		ServiceUsers.closeSession();
		Navigator.pop(context);
		Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => WidgetLogin()), (_) => false);
	}
	Widget getIcon(IconData icon)
	{
		return Icon(icon, color: Colors.white);
	}
	Future<void> _buildItems(context) async
	{
		var user 			= await ServiceUsers.getCurrentUser();
		bool authenticated 	= await ServiceUsers.isLoggedIn();
		String token 		= await SB_Settings.getString('token');
		dynamic	address		= await SB_Settings.getObject('current_address');
		var _items 			= <Widget>[];
		_items.add( new UserAccountsDrawerHeader(
			decoration: BoxDecoration(
				color: Colors.transparent,
			),
			accountName: Text(user != null ? user.fullname : 'Invitado', style: TextStyle(color: Colors.white) ),
			accountEmail: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				children: [
					Text(user != null ? user.email : '', style: TextStyle(color: Colors.white)),
					if( address != null )
						SizedBox(height: 8), Text(address != null ? address['address'] : '', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))
				]
			),
			currentAccountPicture: Row(
				children: [
					Expanded(
						child: Center(
							child: CircleAvatar(
								backgroundImage: user != null && user.avatar.isNotEmpty ? NetworkImage(user.avatar) : AssetImage('images/no-image.jpg'),
								radius: 50
							)
						)
					),
				]
			),
			
			/*
			decoration: BoxDecoration(
				image: DecorationImage(
					image: NetworkImage(user.avatar),
					fit: BoxFit.cover
				)
			)
			*/
		));
		double fontSize = 20;
		var align = TextAlign.left;
		var style = TextStyle(color: Colors.white, fontSize: fontSize);
		_items.addAll([
			new ListTile(
				leading: this.getIcon(Icons.home),
				title: Text('Inicio'.toUpperCase(), textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.of(context).popUntil((route) => route.isFirst);
					Navigator.push(context, MaterialPageRoute(builder: (ctx) => MyHomePage()));
				}
			),
			ListTile(
				leading: this.getIcon(Icons.add_box),
				title: Text('Productos'.toUpperCase(), textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.of(context).popUntil((route) => route.isFirst);
					Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetProducts()));
				}
			),
			ListTile(
				leading: this.getIcon(Icons.local_laundry_service),
				title: Text('Servicios'.toUpperCase(), textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.of(context).popUntil((route) => route.isFirst);
					Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetServices()));
				}
			),
			ListTile(
				leading: this.getIcon(Icons.shopping_cart),
				title: Text('Carrito'.toUpperCase(), textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetCart()));
				}
			),
			
		]);
		if( authenticated )
		{
			/*
			_items.add(new ListTile(
				leading: this.getIcon(Icons.help), 
				title: Text('Ayuda', textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetOrders()));
				}
			));
			_items.add(new ListTile(
				leading: this.getIcon(Icons.my_location),
				title: Text('Mi Dirección', textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetMap()));
				}
			));
			*/
			_items.add(
				ListTile(
					leading: this.getIcon(Icons.calendar_today),
					title: Text('Mis Reservas'.toUpperCase(), textAlign: align, style: style),
					onTap: ()
					{
						Navigator.pop(context);
						//Navigator.of(context).popUntil((route) => route.isFirst);
						Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetAppointments()));
					}
				),
			);
			_items.add(
				ListTile(
					leading: this.getIcon(Icons.format_list_bulleted),
					title: Text('Mis Pedidos'.toUpperCase(), textAlign: align, style: style),
					onTap: ()
					{
						Navigator.pop(context);
						//Navigator.of(context).popUntil((route) => route.isFirst);
						Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetOrders()));
					}
				),
			);
			/*
			ListTile(
				leading: this.getIcon(Icons.settings),
				title: Text('Configuración'.toUpperCase(), textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.of(context).popUntil((route) => route.isFirst);
					Navigator.push(context, MaterialPageRoute(builder: (ctx) => MyHomePage()));
				}
			),
			*/
				_items.add(new ListTile(
					leading: this.getIcon(Icons.my_location),
					title: Text('Mi Ubicación'.toUpperCase(), textAlign: align, style: style),
					onTap: ()
					{
						Navigator.pop(context);
						Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetMyLocation()));
					}
				));
			_items.add(new ListTile(
				leading: this.getIcon(Icons.account_circle),
				title: Text('Mi Perfil'.toUpperCase(), textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetAccount()));
				}
			));
			_items.add(new ListTile(
				leading: this.getIcon(Icons.power_settings_new), 
				title: Text('Salir'.toUpperCase(), textAlign: align, style: style), 
				onTap: () => this.logout(context)));
		}
		else
		{
			_items.add(new ListTile(
				leading: this.getIcon(Icons.vpn_key),
				title: Text('Iniciar sesión'.toUpperCase(), textAlign: align, style: style), 
				onTap: () 
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetLogin()));
				}
			));
		}
		this.setState( ()
		{
			this._items = _items;
		});
	}
	@override
	void initState()
	{
		super.initState();
	}	
	@override
	Widget build(BuildContext context)
	{
		this._buildItems(context);
		return Theme(
			data: Theme.of(context).copyWith(
				canvasColor: Color(0xff000000).withOpacity(0.5),//Colors.blue[900].withOpacity(0.6)
			),
			child: new Drawer(
				child: ListView(
					children: this._items
				)
			)
		);
	}
}
