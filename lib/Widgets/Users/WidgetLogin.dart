import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'WidgetRegister.dart';
import '../../main.dart';
import '../WidgetLoading.dart';
import '../../Services/ServiceUsers.dart';
import '../../Classes/Exceptions/ExceptionLogin.dart';

class WidgetLogin extends StatefulWidget
{
	@override
	WidgetLoginState	createState() => WidgetLoginState();
}
class WidgetLoginState extends State<WidgetLogin>
{
	GoogleSignIn 	gsi = GoogleSignIn(scopes:['email', 'https://www.googleapis.com/auth/contacts.readonly']);
	int		_showIndex 						= 0;
	TextEditingController	_ctrlEmail 		= TextEditingController();
	TextEditingController	_ctrlPassword 	= TextEditingController();
	String					_loginError		= '';
	final					_formKey		= GlobalKey<FormState>();
	void _login()
	{
		if( !this._formKey.currentState.validate() )
			return;
			
		final _key = GlobalKey<State>();
		WidgetLoading.show(context, _key, 'Verificando información...');
		ServiceUsers.login(this._ctrlEmail.text.trim(), this._ctrlPassword.text)
			.then((user)
			{
				WidgetLoading.hide(_key);
				Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => MyHomePage()), (_) => false);
			})
			.catchError((e)
			{
				WidgetLoading.hide(_key);
				print(e.getMessage());
				var alert = AlertDialog(
					title: Text("Error en inicio de sesión"),
					content: Text(e.getMessage()),
					actions: <Widget>[
						FlatButton(
							child: Text("Cerrar"), 
							onPressed: ()
							{
								Navigator.of(context).pop();
							}
						)
					]
				);
				showDialog(context: context, builder: (BuildContext ctx){return alert;});
			}, test: (e) => e is ExceptionLogin)
			.catchError((e)
			{
				WidgetLoading.hide(_key);
				print(e);
				var alert = AlertDialog(
					title: Text("Error en inicio de sesión"),
					content: Text('Ocurrio un error desconocido al iniciar la session, intentelo nuevamente'),
					actions: <Widget>[
						FlatButton(
							child: Text("Cerrar"), 
							onPressed: ()
							{
								Navigator.of(context).pop();
							}
						)
					]
				);
				showDialog(context: context, builder: (BuildContext ctx){return alert;});
			});
		
	}
	void _facebookLogin() async
	{
		final facebookLogin 		= FacebookLogin();
		facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
		final result = await facebookLogin.logIn(['email']);
		switch (result.status) 
		{
			case FacebookLoginStatus.loggedIn:
				print('Token: ' + result.accessToken.token);
				String fb_token = result.accessToken.token;
				//##get facebook user data
				final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${fb_token}');
				final profile = json.decode(graphResponse.body);
				//print(profile);
				//##get user avatar
				final avatarRes = await http.get('https://graph.facebook.com/'+ profile['id'] +'/picture?type=large&redirect=false&access_token=' + fb_token);
				final avatar = json.decode(avatarRes.body);
				//print(avatar);
				var data = {
					'fb_id': profile['id'],
					'email': profile['email'] != null ? profile['email'] : null,
					'fullname': profile['name'],
					'avatar': avatar['data']['url'],
					'token': fb_token
				};
				print(data);
				//{name: Juan Marcelo Aviles Paco, first_name: Juan Marcelo, last_name: Aviles Paco, email: marce_nickya@yahoo.es, id: 2982572738452325}
				var _key = GlobalKey<State>();
				WidgetLoading.show(context, _key, 'Validando información');
				ServiceUsers.fbLogin(data)
					.then( (user) 
					{
						WidgetLoading.hide(_key);
						Navigator.pop(context);
						//Navigator.of(context).popUntil((route) => route.isFirst);
						//Navigator.pushReplacement(context, MaterialPageRoute(builder: (ctx) => MyHomePage()));
						Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (ctx) => MyHomePage()), (_) => false);
					})
					.catchError( (e) 
					{
						WidgetLoading.hide(_key);
						print('FB LOGIN ERROR: ');
						print(e);
						var alert = AlertDialog(
							title: Text("Error en inicio de sesión"),
							content: Text("Ocurrio un error al inciar la sesión, intentelo más adelante."),
							actions: <Widget>[
								FlatButton(
									child: Text("Cerrar"), 
									onPressed: ()
									{
										Navigator.of(context).pop();
									}
								)
							]
						);
						showDialog(context: context, builder: (BuildContext ctx){return alert;});
					});
				
		    break;
		  	case FacebookLoginStatus.cancelledByUser:
		    	//_showCancelledMessage();
		    break;
		  	case FacebookLoginStatus.error:
				print('ERROR: ' + result.errorMessage);
		    	//_showErrorOnUI(result.errorMessage);
		    break;
		}
	}
	Future<void> _googleLogin() async
	{
		try
		{
			await gsi.signIn();
		}
		catch(error)
		{
			print(error);
		}
	}
	void _appleSignIn() async
	{
		try
		{
			final credentials = await SignInWithApple.getAppleIDCredential(
				scopes: [AppleIDAuthorizationScopes.email, AppleIDAuthorizationScopes.fullName]
			);
			var data = {
				'apple_id': credentials.userIdentifier,
				'email': 	credentials.email,
				'fullname': '${credentials.givenName} ${credentials.familyName}',
				'avatar':	null,
				'token': 	credentials.identityToken
			};
			var _key = GlobalKey<State>();
			WidgetLoading.show(context, _key, 'Validando información...');
			ServiceUsers.appleLogin(data).then( (user) 
			{
				WidgetLoading.hide(_key);
				Navigator.pop(context);
				Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (ctx) => MyHomePage()), (_) => false);
			})
			.catchError((e)
			{
				WidgetLoading.hide(_key);
				print(e);
			});
		}
		catch(e)
		{
			print('ERROR');
			print(e);
		}
	}
	Widget _getLoginForm()
	{
		return Form(
			key: this._formKey,
			child: Container(
				padding: EdgeInsets.all(20),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.center,
					children: [
						TextFormField(
							controller: this._ctrlEmail,
							style: TextStyle(color: Colors.white),
							decoration: InputDecoration(
								hintText: 'Usuario/Email',
								hintStyle: TextStyle(color: Colors.white),
								enabledBorder: OutlineInputBorder(
									borderSide: BorderSide(
										width: 2,
										color: Colors.white
									)
								)
							),
							keyboardType: TextInputType.emailAddress,
							validator: (usr)
							{
								if( usr.isEmpty )
									return 'Debe ingresar un nombre de usuario válido';
							},
						),
						SizedBox(height: 10),
						TextFormField(
							controller: this._ctrlPassword,
							style: TextStyle(color: Colors.white),
							decoration: InputDecoration(
								hintText: 'Contraseña',
								hintStyle: TextStyle(color: Colors.white),
								enabledBorder: OutlineInputBorder(
									borderSide: BorderSide(
										width: 2,
										color: Colors.white
									)
								)
							),
							obscureText: true,
							validator: (pass)
							{
								if( pass.isEmpty )
									return 'Debe ingresar su contraseña';
							}
						),
					]
				)
			),
		);	
	}
	Widget _getRegisterButton()
	{
		return Container(
			//color: Colors.red,
			child: Center(
				child: InkWell(
					child: Container(
						padding: EdgeInsets.all(8),
						decoration: BoxDecoration(
							//color: Colors.red,
							border: Border.all(color: Colors.white, style: BorderStyle.solid, width: 2),
							borderRadius: BorderRadius.circular(5),
						),
						child: Text('Registrarse', style: TextStyle(color: Colors.white, fontSize: 25),),
					),
					onTap: ()
					{
						Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetRegister()));
					}
				)
			)
		);
	}
	@override
	void initState()
	{
		super.initState();
		this.gsi.onCurrentUserChanged.listen( (GoogleSignInAccount account) 
		{
			var data = {
				'google_id': account.id,
				'email': 	account.email != null ? account.email : null,
				'fullname': account.displayName,
				'avatar':	account.photoUrl,
				//'token': 	fb_token
			};
			
			if( account != null )
			{
				account.authHeaders.then( (h) 
				{
					//print(h);
					data['token'] = h['Authorization'].toString().replaceAll('Bearer', '').trim();
					print(data);
					final GlobalKey<State> _key = GlobalKey<State>();
					WidgetLoading.show(context, _key, 'Verificando información...');
					//##send data to server
					ServiceUsers.googleLogin(data)
						.then( (user) 
						{
							WidgetLoading.hide(_key);
							Navigator.pop(context);
							Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (ctx) => MyHomePage()), (_) => false);
						})
						.catchError( (e) 
						{
							print(e);
							WidgetLoading.hide(_key);
							var alert = AlertDialog(
								title: Text("Error en inicio de sesión"),
								content: Text("Ocurrio un error al inciar la sesión, intentelo más adelante."),
								actions: <Widget>[
									FlatButton(
										child: Text("Cerrar"), 
										onPressed: ()
										{
											Navigator.of(context).pop();
										}
									)
								]
							);
							showDialog(context: context, builder: (BuildContext ctx){return alert;});
						});
				});
				
			}
		});
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			backgroundColor: Colors.black87,
			body: SafeArea(
				child: Container(
					decoration: BoxDecoration(
						image: DecorationImage(image: AssetImage('images/bg-01.jpg'), fit: BoxFit.cover,)
					),
					child: Container(
						color: Color(0xbb000000),
						child: Column(
							//crossAxisAlignment: CrossAxisAlignment.stretch,
							mainAxisAlignment: MainAxisAlignment.end,
							children: [
								IndexedStack(
									index: this._showIndex,
									children: <Widget>[
										this._getRegisterButton(),
										AnimatedContainer(
											//color: Colors.red,
											duration: Duration(milliseconds: 200),
											//width: this._showIndex == 0 ? 0 : 170,
											height: this._showIndex == 0 ? 0 : 190,
											child: Center(
												child: this._getLoginForm()
											)
										),
									],
								),
								SizedBox(height: 10),
								Center(
									child: InkWell(
										child: Container(
											padding: EdgeInsets.all(8),
											child: Text('Iniciar Sesión', style: TextStyle(color: Colors.white, fontSize: 25),),
										),
										onTap: ()
										{
											if( this._showIndex == 1 )
											{
												//do login
												this._login();
											}
											else
											{
												this.setState(()
												{
													this._showIndex = 1;
												});
											}
											
										},
									)
								),
								SizedBox(height: 10),
								if( this._showIndex == 1 )
									this._getRegisterButton(), 
								SizedBox(height: 10),
								Container(
									child: Text('Inicia sesión con', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold))
								),
								SizedBox(height: 10),
								if( Platform.isIOS )
									Column(
										children: [
											InkWell(
												child: Container(
													width: 240,
													height: 40,
													margin: EdgeInsets.only(right: 5, left: 5),
													decoration: BoxDecoration(
														color: Colors.white,
														border: Border.all(
															color: Colors.white,
															width: 2
														),
														borderRadius: BorderRadius.circular(4),
													),
													child: Row(
														children:[
															SizedBox(width: 10),
															Icon(
																//padding: EdgeInsets.all(0),
																FontAwesomeIcons.apple,
																size: 20,
																color: Colors.black,
															),
															SizedBox(width: 10),
															Text('Iniciar sesión con Apple', style: TextStyle(color: Colors.black, fontSize: 18))
														]
													),
												),
												onTap: () => this._appleSignIn(),
											),
											SizedBox(height: 10),
											Text('ó', style: TextStyle(color: Colors.white, fontSize: 18)),
											SizedBox(height: 10),
										]
									),
								Row(
									children: [
										SizedBox(width: 10),
										Expanded(child: Divider(height: 10,thickness: 2, color: Colors.white, ),),
										SizedBox(width: 5),
										Container(
											width: 35,
											height: 35,
											margin: EdgeInsets.only(right: 5, left: 5),
											decoration: BoxDecoration(
												border: Border.all(
													color: Colors.white,
													width: 2
												),
												borderRadius: BorderRadius.circular(4),
											),
											child: IconButton(
												padding: EdgeInsets.all(0),
												icon: FaIcon(FontAwesomeIcons.facebookF,),
												iconSize: 17,
												color: Colors.white,
												onPressed: ()
												{
													this._facebookLogin();
												},
											)
										),
										Container(
											width: 35,
											height: 35,
											margin: EdgeInsets.only(right: 5, left: 5),
											decoration: BoxDecoration(
												border: Border.all(
													color: Colors.white,
													width: 2
												),
												borderRadius: BorderRadius.circular(4),
											),
											child: IconButton(
												padding: EdgeInsets.all(0),
												icon: FaIcon(FontAwesomeIcons.googlePlusG),
												iconSize: 18,
												color: Colors.white,
												onPressed: ()
												{
													this._googleLogin();
												}
											),
										),
										SizedBox(width: 5),
										Expanded(child: Divider(height: 10,thickness: 2, color: Colors.white, ),),
										SizedBox(width: 10),
									]
								),
								SizedBox(height: 10),
							]
						)
					)
				),
			)
		);
	}
}