import 'package:flutter/material.dart';
import 'WidgetProfile.dart';
import 'WidgetChangePassword.dart';
import '../../Classes/SB_Globals.dart';

class WidgetAccount extends StatefulWidget
{
	@override
	WidgetAccountState	createState() => WidgetAccountState();
}
class WidgetAccountState extends State<WidgetAccount> with RouteAware
{
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Mi cuenta')),
			body: ListView(
				children:[
					ListTile(
						leading: Icon(Icons.assignment_ind),
						title: Text('Información personal'),
						trailing: Icon(Icons.chevron_right),
						onTap:(){
							Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetProfile()));
						}
					),
					ListTile(
						leading: Icon(Icons.https),
						title: Text('Cambiar contraseña'),
						trailing: Icon(Icons.chevron_right),
						onTap:()
						{
							Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetChangePassword()));
						}
					)
				]
			)
		);
	}
	@override
	void didChangeDependencies()
	{
		super.didChangeDependencies();
		print('WidgetAccount -> didChangeDependencies');
		if( SB_Globals.getVar('floating_avatar') != null )
		{
			(SB_Globals.getVar('floating_avatar') as OverlayEntry).remove();
			SB_Globals.setVar('floating_avatar', null);
		}
	}
}