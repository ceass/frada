import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../Classes/WPAppointment.dart';
import '../../Classes/WPUser.dart';
import '../../Services/WpService.dart';
import '../../Services/ServiceUsers.dart';
import '../WidgetStatus.dart';

class WidgetAppointments extends StatefulWidget
{
	@override
	_WidgetAppointmentsState	createState() => _WidgetAppointmentsState();
}
class _WidgetAppointmentsState extends State<WidgetAppointments>
{
	WPUser			_user;
	List<WPAppointment>		_items = [];
	ScrollController		_ctrlScroll;
	int						_currentPage = 1;
	void _scrollListener()
	{
		if( this._ctrlScroll.offset >= this._ctrlScroll.position.maxScrollExtent && !this._ctrlScroll.position.outOfRange )
		{
			print('Appointments bottom');
			WpService.getUserAppointments(this._user, this._currentPage + 1).then((items)
			{
				this.setState(()
				{
					this._items += items;
				});
				
			});
		}	
	}
	@override
	void initState()
	{
		ServiceUsers.getCurrentUser().then((usr)
		{
			this._user = usr;
		});
		this._ctrlScroll = ScrollController();
		this._ctrlScroll.addListener(this._scrollListener);
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Mis citas')),
			 body: Container(
				child: FutureBuilder(
					future: WpService.getUserAppointments(this._user),
					builder: (_, snapshot)
					{
						if( snapshot.hasData )
						{
							this._items = snapshot.data;
							var df = DateFormat('dd-MM-yyyy HH:mm');
							
							return ListView.builder(
								scrollDirection: Axis.vertical,
								controller: this._ctrlScroll,
								itemCount: this._items.length,
								itemBuilder: (ctx, index)
								{
									var ap = (this._items[index] as WPAppointment);
									return Card(
										child: Container(
											padding: EdgeInsets.all(10),
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.stretch,
												children: [
													Text(ap.service_name, style: TextStyle(fontSize: 20)),
													SizedBox(height: 10),
													Row(
														children:[
															Expanded(
																child: Text('Consultora'),
															),
															WidgetStatus(status: ap.status,)
														]
													),
													SizedBox(height: 10),
													Row(
														children: [
															Expanded(
																child: Text('Fecha: ' + df.format(ap.init_date)),
															),
															Expanded(
																child: Text('Precio: ' + ap.price.toStringAsFixed(2)),
															)
														]
													)
												]
											)
										)
									);
								},
							);
						}
						else if( snapshot.hasError )
						{
							return Text('Error recuperando sus reservas.');
						}
						return Center(
							child: Column(
								children: [
									Text('Obteniendo historial de citas...'),
									SizedBox(height: 10,),
									CircularProgressIndicator()
								]
							)
						);
					},
				)
			),
		);
	}
}