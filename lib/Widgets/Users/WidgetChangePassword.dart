import 'package:flutter/material.dart';
import '../WidgetButton.dart';
import '../FormGroup.dart';
import '../../Services/ServiceUsers.dart';
import '../WidgetLoading.dart';

class WidgetChangePassword extends StatelessWidget
{
	TextEditingController	_ctrlPass 	= TextEditingController();
	TextEditingController	_ctrlRPass 	= TextEditingController();
	final					_formKey	= GlobalKey<FormState>();
	
	void _doRecover(context)
	{
		if( !this._formKey.currentState.validate() )
			return;
			
		var data = {'password': this._ctrlPass.text, 'rpassword': this._ctrlRPass.text};
		final _key = GlobalKey<State>();
		WidgetLoading.show(context, _key, 'Procesando datos...');
		ServiceUsers.changePass(data)
			.then( (res) 
			{
				WidgetLoading.hide(_key);
				showDialog(context: context,
					builder: (ctx)
					{
						return AlertDialog(
							content: Text('La contraseña se cambio exitosamente'),
							actions: <Widget>[
								FlatButton(
									child: Text('Ok'),
									onPressed: ()
									{
										Navigator.pop(context);
										Navigator.pop(context);
									}
								)
							]
						);
					}
				);
			})
			.catchError( (e) 
			{
				WidgetLoading.hide(_key);
				showDialog(context: context,
					builder: (ctx)
					{
						return AlertDialog(
							content: Text('Ocurrio un error al cambiar la contraseña, intentalo mas tarde'),
							actions: <Widget>[
								FlatButton(
									child: Text('Cerrar'),
									onPressed: ()
									{
										Navigator.pop(context);
									}
								)
							]
						);
					}
				);
			});
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: Text('Cambio de contraseña')
			),
			body: Container(
				padding: EdgeInsets.all(10),
				child: Form(
					key: this._formKey,
					child: ListView(
						children: [
							Text('Ingresa tu nueva contraseña en los siguientes campos, asegúrate que ambas son exactamente iguales', style: TextStyle(fontSize: 15)),
							SizedBox(height:10),
							FormGroup(
								label: 'Contraseña',
								child: TextFormField(
									obscureText: true,
									controller: this._ctrlPass,
									validator: (v)
									{
										if( v.isEmpty )
											return 'Debes ingresar tu contraseña';
									},
								)
							),
							FormGroup(
								label: 'Repite la Contraseña',
								child: TextFormField(
									obscureText: true,
									controller: this._ctrlRPass,
									validator: (v)
									{
										if( v.isEmpty )
											return 'Debes repetir tu contraseña';
										if( v != this._ctrlPass.text )
											return 'Las contraseñas no coinciden';
									},
								)
							),
							WidgetButton(
								text: 'Confirmar',
								type: 'danger',
								color: Color(0xfff2b2b2),
								callback: () { this._doRecover(context);},
							)
						]
					)
				)
			)
		);
	}
}