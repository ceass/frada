import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../Classes/WCOrder.dart';
import '../../Classes/WCOrderItem.dart';
import '../Cart/WidgetPayment.dart';

class WidgetOrderDetails extends StatelessWidget
{
	WCOrder	order;
	
	WidgetOrderDetails({this.order})
	{
	
	}
	List<Widget> getItems()
	{
		var items = <Widget>[];
		this.order.line_items.forEach((item)
		{
			items.add(
				Card(
					child: Container(
						padding: EdgeInsets.all(10),
						child: Column(
							children: [
								Row( children:[
									Expanded(child: Text(item.qty.toString() + ' x ' + item.name) ),
									Text( (item.qty * item.price).toStringAsFixed(2) ),
								]),
							]
						)
					)
				)
			);
		});
		return items;
	}
	@override
	Widget build(BuildContext context)
	{
		var listViewItems = <Widget>[
			Card(
				child: Container(
					padding: EdgeInsets.all(10),
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						children: [
							Row( children:[Text('Pedido: #', style: TextStyle(fontWeight: FontWeight.bold)), Text(this.order.number.toString())]),
							SizedBox(height: 8),
							Row( children:[
								Text('Fecha: ',	style: TextStyle(fontWeight: FontWeight.bold)),
								Text(DateFormat('d MMMM yyyy H:mm').format(this.order.getDate()).toString())
							]),
							SizedBox(height: 8),
							Row( children:[Text('Estado: ', style: TextStyle(fontWeight: FontWeight.bold)), Text(this.order.getStatus())]),
							SizedBox(height: 8),
							Row( children:[Text('Estado del pago: ', style: TextStyle(fontWeight: FontWeight.bold)), Text(this.order.getPaymentStatus())]),
						]
					)
				)
			),
			if( !this.order.set_paid )
				Card(
					child: Container(
						padding: EdgeInsets.all(10),
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.stretch,
							children: [
								Text('Tu pedido aun no ha sido pagado'),
								SizedBox(height: 10),
								Row(
									children:[
										Expanded(
											child: Text('Método de Pago: ${this.order.payment_method_title} ${this.order.payment_method}' )
										),
										SizedBox(width: 10),
										if( this.order.isPaymentMethodCreditCard() )
											Expanded(
												child: FlatButton(
													color: Colors.green,
													child: Text('Realizar Pago', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
													onPressed: ()
													{
														Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetPayment(orderId: this.order.id)));
													},
												)
											),
									]
								)
							]
						)
					)
				),
			Container(
				padding: EdgeInsets.all(10),
				child: Center(
					child:Text('Items', 
						textAlign: TextAlign.center, 
						style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
					) 
				)
			),				
		] + this.getItems();
		return Scaffold(
			//backgroundColor: Color(0xffff5c1e),
			appBar: AppBar(title: Text('Detalles del pedido')),
			body: SafeArea(
				child: Container(
					/*
					decoration: BoxDecoration(
						color: Color(0xfff8ae7d),
						image: DecorationImage(
							image: AssetImage('images/patitas.png'),
							repeat: ImageRepeat.repeat, 
						)
					),
					*/
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						children: [
							Container(
								padding: EdgeInsets.all(20),
								margin: EdgeInsets.only(top:6, right: 3, bottom:6, left: 3),
								decoration: BoxDecoration(
									color: Color(0xffFF4C58),
									borderRadius: BorderRadius.circular(10),
								),
								child: Column(
									children:[
										Row(
											children: [
												Text('Envio: ', style: TextStyle(fontSize:25, color: Colors.white, fontWeight: FontWeight.bold),),
												Expanded(child: Container(
													//height: 60,
													child: Text(
														'' + this.order.shipping_total /*.toStringAsFixed(2)*/ + ' Bs',
														style: TextStyle(fontSize:25, color: Colors.white, fontWeight: FontWeight.bold),
														textAlign: TextAlign.right
													)
												))
											]
										),
										Row(
											children: [
												Text('Total: ', style: TextStyle(fontSize:25, color: Colors.white, fontWeight: FontWeight.bold),),
												Expanded(child: Container(
													//height: 60,
													margin: EdgeInsets.only(top:6, right: 3, bottom:6, left: 3),
													
													child: Text(
														'' + this.order.total/*.toStringAsFixed(2)*/ + ' Bs',
														style: TextStyle(fontSize:25, color: Colors.white, fontWeight: FontWeight.bold),
														textAlign: TextAlign.right
													)
												))
											]
										)
									]
								)
							),
							Expanded(child: ListView(
								children: listViewItems	
							)),
							
						]
					)
				)
			)
		);
	}
}