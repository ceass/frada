import 'package:flutter/material.dart';
import '../../Services/WooService.dart';
import 'WidgetOrderListItem.dart';

class WidgetOrdersHistory extends StatelessWidget
{
	@override
	Widget build(BuildContext context)
	{
		var orders = WooService.getOrders();
		return FutureBuilder<List>(
			future: orders,
			builder: (context, snapshot)
			{
				if( snapshot.hasData )
				{
					var items = <Widget>[];
					snapshot.data.forEach( (order)
					{
						items.add( WidgetOrderListItem( order: order) );
					});
					if( items.length <= 0 )
					{
						items.add( Container(
							padding: EdgeInsets.all(20),
							child: Center(child: Text('Aun no tienes pedidos', style: TextStyle(fontSize:20))) 
						));
					}
					return ListView(
						children: items
					);
				}
				else if( snapshot.hasError )
				{
					return Center(child: Text('Ocurrio un error al recuperar los pedidos'));
				}
				return Center(child: CircularProgressIndicator());
			}
		);
	}
}
