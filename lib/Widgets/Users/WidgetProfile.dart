import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';
import '../FormGroup.dart';
import '../../Services/ServiceUsers.dart';
import '../../Classes/WPUser.dart';
import '../../Classes/SB_Settings.dart';
import '../WidgetLoading.dart';

class WidgetProfile extends StatefulWidget
{
	@override
	_WidgetProfileState	createState() => _WidgetProfileState();
}
class _WidgetProfileState extends State<WidgetProfile>
{
	File	_image;
	TextEditingController	_ctrlName 		= TextEditingController();
	TextEditingController	_ctrlLastname 	= TextEditingController();
	TextEditingController	_ctrlPhone		= TextEditingController();
	TextEditingController	_ctrlEmail		= TextEditingController();
	final					_formKey		= GlobalKey<FormState>();
	WPUser					_user;
	Image					_avatarImage;
	
	Future _selectImage() async
	{
		this._image = await ImagePicker.pickImage(source: ImageSource.gallery);
		this.setState(()
		{
			this._avatarImage = Image.file(this._image, width:120, height:120, fit: BoxFit.fill);
		});
	}
	void _saveData(context)
	{
		if( !this._formKey.currentState.validate() )
			return;
		var user 			= {};
		user['id'] 			= this._user.id;
		//user.first_name = this._ctrlName.text;
		//user.last_name 	= this._ctrlLastname.text;
		user['email']		= this._ctrlEmail.text.trim();
		user['first_name']	= this._ctrlName.text.trim();
		user['last_name']	= this._ctrlLastname.text.trim();
		user['meta']		= [
			{'key': '_phone'},
			{'value': this._ctrlPhone.text.trim()}	
		];
		final _key = GlobalKey<State>();
		WidgetLoading.show(context, _key, 'Guardando datos...');
		ServiceUsers.update(user)
			.then( (v) 
			{
				ServiceUsers.reload();
				if( this._image != null )
				{
					print('Uploading avatar');
					ServiceUsers.uploadAvatar(this._image).then( (res) 
					{
						print(res);
						WidgetLoading.hide(_key);
						//##update user into session
						
					})
					.catchError((e)
					{
						print(e);
						WidgetLoading.hide(_key);
						
					});
				}
				else
				{
					WidgetLoading.hide(_key);
				}
			})
			.catchError( (e) 
			{
				WidgetLoading.hide(_key);
				print(e);
			});
		
		
	}
	Widget _getForm(context)
	{
		return Form(
			key: this._formKey,
			child: Column(
				children: [
					FormGroup(
						label: 'Nombre',
						child: TextFormField(
							controller: this._ctrlName,
							validator: (value)
							{
								if( value.isEmpty )
									return 'Debe ingresar su nombre';
							}
						)
					),
					FormGroup(
						label: 'Apellido',
						child: TextFormField(
							controller: this._ctrlLastname,
							validator: (value)
							{
								if( value.isEmpty )
									return 'Debe ingresar su apellido';
							}
						)
					),
					FormGroup(
						label: 'Teléfono',
						child: TextFormField(
							keyboardType: TextInputType.phone,
							controller: this._ctrlPhone,
							validator: (value)
							{
								if( value.isEmpty )
									return 'Debe ingresar su número de telefono';
							}
						)
					),
					FormGroup(
						label: 'Email',
						child: TextFormField(
							keyboardType: TextInputType.emailAddress,
							controller: this._ctrlEmail,
							validator: (value)
							{
								//return 'Debe ingresar su direccion de email';
							}	
						)
					)
				]
			)
		);
	}
	@override
	void initState()
	{
		super.initState();
		
		//this._avatarImage != null ? Image.file(this._image, width: 120, height: 120, fit: BoxFit.fill,) : 
		ServiceUsers.me().then( (user) 
		{	
			this._ctrlName.text		= user.first_name;
			this._ctrlLastname.text	= user.last_name;
			this._ctrlPhone.text	= user.meta['_phone'] != null ? user.meta['_phone'] : '';
			this._ctrlEmail.text 	= user.email;
			//print(this._user.toMap());
			Image image = null;
			if( !user.avatar.isEmpty )
			{
				//print(this._user.avatar);
				image = Image.network(user.avatar, width:120, height:120, fit: BoxFit.fill);
			}
			else
			{
				image = Image.asset('images/empty-avatar.png', width:120, height:120, fit: BoxFit.fill);
			}
			this.setState(()
			{
				this._avatarImage = image;
				this._user 				= user;
			});
				
		});
	}
	@override
	Widget build(BuildContext context)
	{
		
		return Scaffold(
			appBar: AppBar(title: Text('Información personal')),
			floatingActionButton: FloatingActionButton(
				onPressed: ()
				{
					this._saveData(context);
				},
				tooltip: 'Guardar datos',
				child: Icon(Icons.save),
			),
			body: Container(
				padding: EdgeInsets.all(5),
				child: ListView(
					children: [
						SizedBox(height:20),
						Center(
							child: InkWell(
								child: Container(
									padding: EdgeInsets.all(10),
									decoration: BoxDecoration(
										borderRadius: BorderRadius.all(Radius.circular(80.0)),
										color: Colors.white,
										border: Border.all(color: Colors.grey[300]),
									),
									child: ClipRRect(
										borderRadius: BorderRadius.circular(80),
										child: this._avatarImage
									)
								),
								onTap: ()
								{
									this._selectImage();
								}
							) 
						),
						SizedBox(height:20),
						Card(
							child: Container(
								padding: EdgeInsets.all(10),
								child: this._getForm(context),
							)
						),
						
					]
				)
			)
		);
	}
}