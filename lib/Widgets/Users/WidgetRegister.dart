import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../Services/ServiceUsers.dart';
import '../../Services/Service.dart';
import '../WidgetLoading.dart';

class WidgetRegister extends StatelessWidget
{
	final					_formKey	= GlobalKey<FormState>();
	TextEditingController	_ctrlName 	= TextEditingController();
	TextEditingController	_ctrlEmail	= TextEditingController();
	TextEditingController	_ctrlPass	= TextEditingController();
	TextEditingController	_ctrlRPass	= TextEditingController();
	
	Widget _buildTextField(String label, TextInputType type, TextEditingController ctrl, [bool obscure = false, Function validator = null])
	{
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			children: [
				Text(label, style: TextStyle(color: Colors.white)),
				SizedBox(height: 5),
				TextFormField(
					style: TextStyle(color: Colors.white),
					keyboardType: type,
					controller: ctrl,
					obscureText: obscure,
					decoration: InputDecoration(
						filled: true,
						fillColor: Color(0xff424242),
						enabledBorder: OutlineInputBorder(
							borderSide: BorderSide(color: Colors.white70, width: 1)
						)
					),
					validator: validator,
				)
			]
		);
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			backgroundColor: Colors.black,
			body: Container(
				child: ListView(
					children: <Widget>[
						Container(
							child: Stack(
								children:[
									Image.asset('images/productos.jpg'),
									Positioned(
										right: 0,
										bottom: 0,
										left: 0,
										child: Container(
											padding: EdgeInsets.all(10),
											color: Color(0xfff2b2b2),
											child: Text('Cuenta Nueva', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 20))
										)
									)
								]
							)
						),
						SizedBox(height: 10),
						Container(
							padding: EdgeInsets.all(10),
							child: Form(
								key: this._formKey,
								child: Column(
									children: [
										this._buildTextField('Nombre', TextInputType.text, this._ctrlName,
											false,
											(String val)
											{
												if( val.isEmpty )
													return 'Debe ingresar su nombre';
											}
										),
										SizedBox(height: 10),
										this._buildTextField('Email', TextInputType.text, this._ctrlEmail,
											false,
											(String email)
											{
												if( email.isEmpty || email.indexOf('@') == -1 )
													return 'Debe ingresar su direccion de correo';
											}
										),
										SizedBox(height: 10),
										this._buildTextField('Contraseña', TextInputType.text, this._ctrlPass, true,
											(String pass)
											{
												if( pass.isEmpty )
													return 'Debe ingresar una contraseña válida';
											} 
										),
										SizedBox(height: 10),
										this._buildTextField('Confirmar Contraseña', TextInputType.text, this._ctrlRPass, true,
											(String rpass)
											{
												if( rpass.isEmpty )
													return 'Debe repetir su contraseña';
												if( rpass != this._ctrlPass.text )
													return 'Las contraseñas no coinciden';
											}
										),
									]
								),
							)
						),
						SizedBox(height: 30),
						Row(
							children: [
								Expanded(
									child: Center(
										child: InkWell(
											child: Container(
												padding: EdgeInsets.all(15),
												width: 130,
												decoration: BoxDecoration(
													color: Color(0xffd8d4d0),
												),
												child: Text('Cancelar', textAlign: TextAlign.center, style: TextStyle(color: Colors.white))
											),
											onTap: ()
											{
												Navigator.pop(context);
											},
										)
									)
								),
								SizedBox(width: 10),
								Expanded(
									child: Center(
										child: InkWell(
											child: Container(
												padding: EdgeInsets.all(15),
												width: 130,
												decoration: BoxDecoration(
													color: Color(0xfff2b2b2),
												),
												child: Text('Crear Cuenta', textAlign: TextAlign.center, style: TextStyle(color: Colors.white))
											),
											onTap: ()
											{
												this._register(context);
											}
										),
									)
								)
							]
						),
						SizedBox(height: 10),
						Center(
							child: Text('Al crear una cuenta aceptas nuestros terminos y condiciones', textAlign: TextAlign.center, style: TextStyle(color: Colors.white))
						)
					],
				)
			),
		);
	}
	void _register(context)
	{
		if( !this._formKey.currentState.validate() )
			return;
		var user = {
			'username':		this._ctrlEmail.text.trim(),
			'fullname':		this._ctrlName.text.trim(),
			'email':		this._ctrlEmail.text.trim(),
			'password':		this._ctrlPass.text,
		};
		print(user);
		final _key = GlobalKey<State>();
		WidgetLoading.show(context, _key, 'Procesando el registro...');
		ServiceUsers.register(user)
			.then((newUser)
			{
				WidgetLoading.hide(_key);
				showDialog(
					context: context,
					builder: (_)
					{
						return AlertDialog(
							title: Text('Registro exitoso'),
							content: SingleChildScrollView(
								child: ListBody(
									children: <Widget>[
										Center(child: Image.asset('images/success-img.png')),
										SizedBox(height: 10),
										Text('Faclicidades!!'),
										SizedBox(height: 10),
										Text('Su registro ha sido completado exitosamente, ahora puede iniciar sesion para disfrutar de nuestros productos y servicios.'),
										SizedBox(height: 10),
										FlatButton(
											child: Text('Continuar', style: TextStyle(color: Colors.white)),
											color: Color(0xffFF4C58),
											onPressed: ()
											{
												Navigator.pop(context);
												Navigator.pop(context);
											}
										)
									],
								),
							),
						);
					}
				);
			})
			.catchError((e)
			{
				WidgetLoading.hide(_key);
				print(e.getErrorMap());
				var data = e.getErrorMap();
				showDialog(
					context: context,
					builder: (_)
					{
						return AlertDialog(
							title: Text('Error de registro'),
							content: SingleChildScrollView(
								child: ListBody(
									children: <Widget>[
										Center(child: FaIcon(FontAwesomeIcons.timesCircle, color: Colors.red, size: 50,)),
										SizedBox(height: 10),
										Text(data['message']),
										SizedBox(height: 10),
										FlatButton(
											child: Text('Cerrar', style: TextStyle(color: Colors.white)),
											color: Color(0xffFF4C58),
											onPressed: ()
											{
												Navigator.pop(context);
											}
										)
									],
								),
							),
						);
					}
				);
				
			}, test: (_e) => _e is RequestException)
			.catchError((e)
			{
				WidgetLoading.hide(_key);
				showDialog(
					context: context,
					builder: (_)
					{
						return AlertDialog(
							title: Text('Error de registro'),
							content: SingleChildScrollView(
								child: ListBody(
									children: <Widget>[
										Center(child: FaIcon(FontAwesomeIcons.timesCircle, color: Colors.red, size: 50,)),
										SizedBox(height: 10),
										Text('Lo sentimos!!'),
										Text('Ocurrio un error en el proceso de registro, porfavor intentalo nuevamente.'),
										SizedBox(height: 10),
										FlatButton(
											child: Text('Cerrar', style: TextStyle(color: Colors.white)),
											color: Color(0xffFF4C58),
											onPressed: ()
											{
												Navigator.pop(context);
											}
										)
									],
								),
							),
						);
					}
				);
			});
	}
}