import 'package:flutter/material.dart';
import '../Classes/SB_Cart.dart';
import '../main.dart';
import 'Services/WidgetServices.dart';
import 'Products/WidgetProducts.dart';
import 'Cart/WidgetCart.dart';

class WidgetBottomMenu extends StatefulWidget
{
	final int currentIndex;
	WidgetBottomMenu({this.currentIndex = 0});
	@override
	_WidgetBottomMenuState	createState() => _WidgetBottomMenuState();
}
class _WidgetBottomMenuState extends State<WidgetBottomMenu>
{
	SB_Cart			_cart = SB_Cart();
	Widget			_cartIcon = Icon(Icons.shopping_cart, color: Colors.white);
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		Widget menu = BottomNavigationBar(
			type: BottomNavigationBarType.fixed,
			backgroundColor: Color(0xfff2b2b2),
			unselectedItemColor: Color(0xffd8d4d0),
			selectedItemColor: Colors.black,
			currentIndex: this.widget.currentIndex,
			onTap: (int index)
			{
				if( index == 0 )
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => MyHomePage()));
				}
				else if( index == 1 )
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetProducts()));
				}
				else if( index == 2 )
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetServices()));
				}
				else if( index == 3 )
				{
					Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetCart()));
				}
			},
			items: <BottomNavigationBarItem>[
				BottomNavigationBarItem(
					backgroundColor: Color(0xfff2b2b2),
					icon: Icon(Icons.home),
					title: Text('Inicio'),
				),
				BottomNavigationBarItem(
					icon: Icon(Icons.local_grocery_store),
					title: Text('Productos'),
				),
				BottomNavigationBarItem(
					icon: Icon(Icons.flight_land),
					title: Text('Servicios'),
				),
				BottomNavigationBarItem(
					icon: Icon(Icons.shopping_cart),
					title: Text('Carrito'),
				),
			]
		);
		return Theme(
			data: Theme.of(context).copyWith(
				textTheme: Theme.of(context).textTheme,
				bottomAppBarTheme: Theme.of(context).bottomAppBarTheme,
				bottomAppBarColor: Colors.red
			),
			child: menu, 
		);
	}
}