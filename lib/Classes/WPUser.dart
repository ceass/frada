class WPUser
{
	int						ID;
	String					user_login;
	String					user_email;
	String					user_status;
	String					display_name;
	String					user_nicename;
	String					avatar;
	String					first_name;
	String					last_name;
	Map<dynamic, dynamic>	meta = {};
	List					roles = [];
	
	int get id
	{
		return this.ID;
	}
	void set id(int _id) 
	{
		this.ID = _id;
	}
	String get fullname
	{
		return this.display_name;
	}
	String get firstname
	{
		return this.first_name;
	}
	String get lastname
	{
		return this.last_name;
	}
	String get email
	{
		return this.user_email;
	}
	void set email(String e)
	{
		this.user_email = e;
	}
	String get city
	{
		return this.meta['shipping_city'] ?? '';
	}
	String get address
	{
		return this.meta['shipping_address_1'] ?? '';
	}
	String get description
	{
		String desc = this.meta['description'] ?? '';
		if( desc == null || desc.isEmpty )
			return 'El consultor aun no tiene descripción';
			
		return desc.replaceAll('\n', '<br/>');
	}
	WPUser();
	WPUser.fromJson(Map<dynamic, dynamic> data)
	{
		int _id			= 0;
		if( data['ID'] != null )
		{
			_id = int.parse(data['ID'].toString());
		}
		if( data['id'] != null )
			_id = int.parse(data['id'].toString());
			
		this.ID 			= _id;
		this.user_login 	= data['user_login'];
		this.user_email 	= data['user_email'];
		this.user_status	= data['user_status'] ?? '';
		this.display_name	= data['display_name'] ?? '';
		this.user_nicename	= data['user_nicename'];
		this.first_name		= data['first_name'] ?? '';
		this.last_name		= data['last_name'] ?? '';
		this.avatar			= '';
		if( data['simple_local_avatar'] != null )
		{
			if( (data['simple_local_avatar'] is Map) )
			{
				if( data['simple_local_avatar']['96'] != null )
					this.avatar = data['simple_local_avatar']['96'];
				if( data['simple_local_avatar']['full'] != null )
					this.avatar = data['simple_local_avatar']['full'];
			}
		}
		if( data['avatar'] != null )
		{
			this.avatar = data['avatar'];
		}
		this.meta			= data['meta'] ?? {};
		this.roles			= data['roles'] ?? [];
	}
	Map<String, dynamic> toMap()
	{
		var data = {
			'ID': 				this.id,
			'user_login': 		this.user_login,
			'user_email': 		this.user_email,
			'user_status': 		this.user_status,
			'display_name': 	this.display_name,
			'user_nicename': 	this.user_nicename,
			'first_name':		this.first_name,
			'last_name':		this.last_name,
			'email':			this.user_email,
			'avatar':			this.avatar,
			'roles':			this.roles,
		};
		
		return data;
	}
}