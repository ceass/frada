import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class SB_Geo
{
	static Future<String> getAddress(String lat, String lng) async
	{
		String endpoint = 'https://nominatim.openstreetmap.org/reverse?'+
							'lat=' + lat +
							'&lon='+ lng +
							'&format=json';
		//print(endpoint);
		final response = await http.get(endpoint);
		if( response.statusCode != 200 )
			throw new Exception('Error al obtener la geo direccion');
		//print(response.body);
		var res = json.decode(response.body);
		
		return res['display_name'];
	}
}
