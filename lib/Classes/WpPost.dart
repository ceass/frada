class WpPost
{
	int id;
	String	date;
	String	slug;
	String	status;
	String	link;
	String 	title;
	String	content;
	String	excerpt;
	String	featuredImage;
	
	String	iconUrl;
	String	color;
	
	WpPost();
	
	WpPost.fromMap(Map data)
	{
		this.loadData(data);
	}
	void loadData(Map data)
	{
		this.id 			= data['id'];
		this.slug			= data['slug'];
		this.status			= data['status'];
		this.link			= data['link'];
		this.title			= data['title']['rendered'];
		this.content		= data['content']['rendered'];
		this.excerpt		= data['excerpt']['rendered'];
		this.featuredImage	= data['featured_image'] != null && data['featured_image'] != false ? data['featured_image'] : '';
		this.date			= data['date'];
		this.iconUrl		= data['icon_url'] ?? '';
		this.color			= data['color'] ?? '';
	}
	String getThumbnail()
	{
		return this.featuredImage;
	}
}