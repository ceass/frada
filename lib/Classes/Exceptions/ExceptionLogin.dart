class ExceptionLogin implements Exception
{
	Map<String, dynamic>	response;
	String _error;
	
	ExceptionLogin(String error, Map response)
	{
		this._error = error;
		this.response = response;
	}
	String erroMsg()
	{
		return this._error;
	}
	Map<String, dynamic> getData() => this.response;
	String getError() => this.response['error'] ?? '';
	String getMessage() => this.response['message'] ?? '';
	String getCode() => this.response['code'] != null ? this.response['code'].toString() : '';
}
class ExceptionRegister implements Exception
{
	Map<String, dynamic>	response;
	String _error;
	
	ExceptionRegister(String error, Map response)
	{
		this._error = error;
		this.response = response;
	}
	String erroMsg()
	{
		return this._error;
	}
	Map<String, dynamic> getData() => this.response;
	String getError() => this.response['error'];
	int getCode() => this.response['code'];
}