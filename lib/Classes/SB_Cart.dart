import 'SB_CartItem.dart';
import 'WCProduct.dart';

class SB_Cart
{
	List<SB_CartItem> _items;
	double	_total = 0;
	static final SB_Cart _instance = SB_Cart._privateConstructor();
	double	deliveryFee = 0;
	
	SB_Cart._privateConstructor()
	{
		this._items = new List();
	}
	factory SB_Cart()
	{
		return _instance;
	}
	SB_CartItem addProduct(WCProduct product, [int qty = 1])
	{
		var cartItem = this.productExists(product.id);
		if( cartItem == null )
		{
			cartItem = new SB_CartItem(product: product, quantity: qty);
			this._items.add( cartItem );
		}
		else
		{
			cartItem.increaseQuantity(qty);
		}
		return cartItem;
	}
	void removeProduct(int productId)
	{
		var toRemove = null;
		this._items.forEach( (item) 
		{
			if( item.product.id == productId )
			{
				toRemove = item;
			}
		});
		if( toRemove != null )
		{
			this._items.remove(toRemove);
		}
	}
	SB_CartItem productExists(int productId)
	{
		var cartItem = null;
		
		this._items.forEach( (item) 
		{
			if( item.product.id == productId )
			{
				cartItem = item;
			}
		});
		
		return cartItem;
	}
	void clear()
	{
		this._items = new List();
	}
	double calculateTotals()
	{
		this._total = 0;
		this._items.forEach( (SB_CartItem item) 
		{
			this._total += item.getTotal();
		});
		
		return this._total;
	}
	double getTotals()
	{
		return this.calculateTotals();
	}
	List getItems()
	{
		return this._items;
	}
	double getDeliveryCost()
	{
		return this.deliveryFee;
	}
}
