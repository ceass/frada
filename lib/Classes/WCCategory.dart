class WCCategory
{
	int			id;
	String		name;
	String		slug;
	int			parent;
	String		description;
	String		display;
	String		image;
	int			menu_order;
	int			count;
	
	WCCategory();
	
	WCCategory.fromMap(Map<dynamic, dynamic> data)
	{
		this.id				= data['id'];
		this.name			= data['name'];
		this.slug			= data['slug'];
		this.parent			= data['parent'];
		this.description	= data['description'];
		this.display		= data['display'];
		this.image			= data['image'];
		this.menu_order		= data['menu_order'];
		this.count			= data['count'];
	}
}