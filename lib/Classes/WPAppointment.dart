import 'package:intl/intl.dart';

class WPAppointment
{
	int			id;
	String		name;
	int			user_id;
	int			object_id;
	String		object_type;
	DateTime	init_date;
	DateTime	end_date;
	String		notes;
	String		status;
	double		price;
	int			assigned_user_id;
	String		address;
	String		service_name;
	String		latitude;
	String		longitude;
	String		phone;
	
	WPAppointment();
	
	WPAppointment.fromMap(Map<dynamic, dynamic> data)
	{
		this.loadData(data);
	}
	void loadData(Map<dynamic, dynamic> data)
	{
		this.id 				= data['id'];
		this.name				= data['name'];
		this.user_id			= int.parse(data['user_id'].toString());
		this.object_id			= data['object_id'];
		this.init_date			= data['init_date'] != null ? DateTime.parse(data['init_date']) : null;
		this.end_date			= data['end_date'] != null ? DateTime.parse(data['end_date']) : null;
		this.notes				= data['notes'];
		this.status				= data['status'];
		this.price				= double.parse(data['price'].toString());
		this.assigned_user_id	= data['assigned_user_id'];
		this.address			= data['address'] ?? '';
		this.service_name		= data['service_name'];
		this.latitude			= data['latitude'];
		this.longitude			= data['longitude'];
	}
	Map<String, dynamic> toMap()
	{
		var df = DateFormat('yyyy-MM-dd HH:mm:ss');
		var data = {
			'id':				this.id,
			'user_id':			this.user_id,
			'object_id':		this.object_id,
			'init_date':		this.init_date != null ? df.format(this.init_date) : null,
			'end_date':			this.end_date != null ? df.format(this.end_date) : null,
			'notes':			this.notes,
			'price':			this.price,
			'assigned_user_id':	this.assigned_user_id,
			'address':			this.address,
			'latitude':			this.latitude,
			'longitude':		this.longitude,
			'phone':			this.phone,
		};
		
		return data;
	}
}