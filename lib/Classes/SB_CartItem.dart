import 'WCProduct.dart';

class SB_CartItem
{
	WCProduct	product;
	int			quantity = 0;
	double		price = 0;
	double		total = 0;
	Map						attributes = {};
	Map<String, dynamic>	meta = {};
	
	SB_CartItem({this.product, this.quantity = 1})
	{
		this.price = this.product.price;
	}
	int setQuantity(int qty)
	{
		this.quantity	= qty;
		this.total 		= (this.quantity * this.price);
	}
	int increaseQuantity([int qty = 1])
	{
		if( qty > 1 )
			this.quantity += qty;
		else
			this.quantity++;
		return this.quantity;
	}
	double getTotal()
	{
		return this.price * this.quantity;
	}
}
