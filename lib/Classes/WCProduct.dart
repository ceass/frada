import 'package:html/parser.dart' as html;

class WCProduct
{
	int		id;
	String	name;
	String	slug;
	String	permalink;
	String	date_created;
	String	date_created_gmt;
	String	type;
	String	status;
	bool	featured;
	String	catalog_visibility;
	String	description;
	String	short_description;
	String	sku;
	double	price;
	double	regular_price;
	double	sale_price;
	String	price_html;
	bool	on_sale;
	int		stock_quantity;
	String	stock_status;
	
	List<dynamic>	images = [];
	List<dynamic>	meta_data = [];
	
	WCProduct();
	WCProduct.fromJson(Map<String, dynamic> data)
	{
		this.loadData(data);
	}
	void loadData(Map<String, dynamic> data)
	{
		this.id					= data['id'];
		this.name				= data['name'];
		this.slug				= data['slug'];
		this.permalink			= data['permalink'];
		this.date_created		= data['date_created'];
		this.date_created_gmt	= data['date_created_gmt'];
		this.type				= data['type'];
		this.status				= data['status'];
		this.featured			= data['featured'];
		this.catalog_visibility	= data['catalog_visibility'];
		this.description		= data['description'];
		this.short_description	= data['short_description'];
		this.sku				= data['sku'];
		this.price				= data['price'] != null && data['price'].toString().isNotEmpty ? double.parse(data['price'].toString()) : 0;
		this.regular_price		= data['regular_price'] != null && data['regular_price'].toString().isNotEmpty ? double.parse(data['regular_price'].toString()) : 0;
		this.sale_price			= data['sale_price'] != null && data['sale_price'].toString().isNotEmpty ? double.parse(data['sale_price'].toString()) : 0;
		this.price_html			= data['price_html'];
		this.on_sale			= data['on_sale'];
		this.stock_quantity		= data['stock_quantity'];
		this.stock_status		= data['stock_status'];
		this.images				= data['images'];
		this.meta_data			= data['meta_data'];
	}
	String getThumbnailUrl()
	{
		if( this.images == null || this.images.length <= 0 )
			return '';
		return this.images[0]['src'];
	}
	String getPrice()
	{
		return 'Bs. ' + this.price.toString();
	}
	String getExcerpt([int length = 150])
	{
		String excerpt = this.short_description.isNotEmpty ? this.short_description : this.description;
		excerpt = html.parse(excerpt).documentElement.text;
		if( excerpt.length <= length )
			return excerpt;
		
		return excerpt.substring(0, length) + '...';
	}
}