class SB_Service
{
	int		id;
	String	name;
	String	description;
	double	price;
	String	image;
	String	color;
	
	SB_Service({this.id, this.name, this.description, this.price, this.image, this.color});
	String getThumbnail()
	{
		return this.image;
	}
}