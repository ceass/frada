class WCShippingMethod
{
	int		id;
	String		title;
	bool		enabled;
	String		method_id;
	String		method_title;
	String		method_description;
	Map			settings;
	
	double get cost
	{
		String cost = this.settings['cost']['value'] ?? '';
		double c = 0;
		if( cost.isEmpty )
			return c;
		return double.parse(cost);
	}
	WCShippingMethod();
	
	WCShippingMethod.fromMap(Map<dynamic, dynamic> data)
	{
		this.id 		= data['id'];
		this.title		= data['title'];
		this.enabled		= data['enabled'];
		this.method_id	= data['method_id'];
		this.method_title	= data['method_title'];
		this.method_description	= data['method_description'];
		this.settings			= data['settings'];
	}
}