import 'SB_CartItem.dart';

class WCOrderItem
{
	int		id;
	String	name;
	int		product_id;
	int		variation_id;
	int		quantity;
	String	tax_class;
	double	subtotal;
	double	subtotal_tax;
	double	total;
	double	total_tax;
	String	sku;
	double	price;
	
	int get qty
	{
		return this.quantity;
	}
	WCOrderItem();
	
	WCOrderItem.fromMap(Map<dynamic, dynamic> data)
	{
		this.loadData(data);
	}
	WCOrderItem.fromCartItem(SB_CartItem item)
	{
		this.quantity	= item.quantity;
		this.price		= item.price;
		this.product_id	= item.product.id;
	}
	void loadData(Map<dynamic, dynamic> data)
	{
		this.id 			= data['id'];
		this.name			= data['name'];
		this.product_id		= data['product_id'];
		this.variation_id	= data['variation_id'];
		this.quantity		= data['quantity'];
		this.subtotal		= data['subtotal'].toString().isNotEmpty ? double.parse(data['subtotal']) : 0;
		this.subtotal_tax	= data['subtotal_tax'].toString().isNotEmpty ? double.parse(data['subtotal_tax']) : 0;
		this.total			= data['total'].toString().isNotEmpty ? double.parse(data['total']) : 0;
		this.total_tax		= data['total_tax'].toString().isNotEmpty ? double.parse(data['total_tax']) : 0;
		this.sku			= data['sku'];
		this.price			= data['price'] != null ? double.parse(data['price'].toString()) : 0; 
	}
	Map<String, dynamic> toMap()
	{
		var data = {
			'product_id': 	this.product_id,
			'quantity': 	this.quantity,
			//'variation_id': this.variation_id,
		};
		
		return data;
	}
}