import 'SB_Cart.dart';
import 'SB_CartItem.dart';
import 'WCOrderItem.dart';

class WCOrder
{
	int			id;
	int 		parent_id;
	int			number;
	String		order_key;
	String		created_via;
	String		version;
	String		status;
	String		currency;
	String		date_created;
	String		date_created_gmt;
	String		date_modified;
	String		date_modified_gmt;
	String		discount_total;
	String		discount_tax;
	String		shipping_total;
	String		shipping_tax;
	String		cart_tax;
	String		total;
	String		total_tax;
	String		prices_include_tax;
	int			customer_id;
	String		customer_ip_address;
	String		customer_user_agent;
	String						customer_note;
	Map<String, dynamic>		billing = {};
	Map<String, dynamic>		shipping = {};
	String						payment_method;
	String						payment_method_title;
	String						transaction_id;
	String						date_paid;
	String						date_paid_gmt;
	String						date_completed;
	String						date_completed_gmt;
	List<dynamic>				meta_data = [];
	List						line_items = [];
	List						tax_lines = [];
	List						shipping_lines = [];
	List						fee_lines = [];
	List						coupon_lines = [];
	List						refunds = [];
	bool						set_paid = false;
	
	WCOrder.fromMap(Map<dynamic, dynamic> data)
	{
		this.loadData(data);
	}
	WCOrder.fromCart(SB_Cart cart)
	{
		this.line_items = [];
		cart.getItems().forEach( (item) 
		{
			var oitem = WCOrderItem.fromCartItem(item);
			this.line_items.add(oitem);
		});
	}
	void loadData(Map<dynamic, dynamic> data)
	{
		this.id 					= data['id'];
		this.number					= int.parse(data['number'].toString());
		this.status					= data['status'];
		this.set_paid				= data['set_paid'] ?? false;
		this.customer_id			= data['customer_id'];
		this.customer_note			= data['customer_name'];
		this.billing				= data['billing'];
		this.shipping				= data['shipping'];
		this.payment_method			= data['payment_method'];
		this.payment_method_title	= data['payment_method_title'];
		this.date_created			= data['date_created'];
		this.date_created_gmt		= data['date_gmt'];
		this.total					= data['total'];
		this.date_paid				= data['date_paid'];
		this.date_paid_gmt			= data['date_paid_gmt'];
		this.shipping_total			= data['shipping_total'] ?? 0;
		print('Setting meta data');
		this.meta_data 		= data['meta_data'];
		if( data['line_items'] != null )
		{
			data['line_items'].forEach((oid)
			{
				this.line_items.add( new WCOrderItem.fromMap(oid));
			});
		}
		
	}
	Map<String, dynamic> toMap()
	{
		var data = {
			//'id':						this.id,
			'payment_method': 			this.payment_method,
			'payment_method_title':		this.payment_method_title,
			'set_paid':					this.set_paid,
			'customer_id':				this.customer_id,
			'billing':					this.billing,
			'shipping':					this.shipping,
			'line_items':				[],
			'shipping_lines':			[],
			'meta_data':				this.meta_data
		};
		this.line_items.forEach((oi)
		{
			(data['line_items'] as List).add((oi as WCOrderItem).toMap());
		});
		return data;
	}
	
	DateTime getDate()
	{
		return DateTime.parse(this.date_created);
	}
	String getStatus()
	{
		return this.getStatuses(this.status);
	}
	String getPaymentStatus()
	{
		return this.getStatuses(this.set_paid ? 'complete' : 'pending');
	}
	dynamic getStatuses([String status = null])
	{
		var statuses = {
			'complete': 'Completado',
			'draft': 'Borrador',
			'pending': 'Pendiente',
			'REVERSED': 'Revertido',
			'CANCELLED': 'Cancelado',
			'VOID': 'Anulado',
			'PROCESSING': 'En Proceso',
			'ON_THE_WAY': 'En camino',
			'DELIVERED': 'Entregado'
		};
		if( status != null )
			return statuses[status];
			
		return statuses;
	}
	List getItems()
	{
		return this.line_items;
	}
	bool isPaymentMethodCreditCard()
	{
		return this.payment_method == 'lckout';
	}
}