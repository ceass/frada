class WCShippingZone
{
	int			id;
	String		name;
	
	WCShippingZone();
	WCShippingZone.fromMap(Map<dynamic, dynamic> data)
	{
		this.id = data['id'];
		this.name = data['name'];
	}
}