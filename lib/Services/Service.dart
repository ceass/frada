import '../Classes/SB_Settings.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class RequestException implements Exception
{
	int statusCode;
	String	message;
	http.Response	response;
	
	RequestException(String message, http.Response response)
	{
		this.message	= message;
		this.response	= response;
		this.statusCode	= this.response.statusCode;
	}
	Map<dynamic, dynamic> getErrorMap()
	{
		return json.decode(this.response.body);
	}
}
class Service
{
	static String baseUrl		= 'https://www.fradaswissco.com';
	static String apiBase		= 'https://www.fradaswissco.com/wp-json';
	
	static Future<Map<String, String>> getHeaders() async
	{
		Map<String, String> headers = {
			'Content-Type': 'application/json',
		};
		String token = await SB_Settings.getString('token');
		if( token != null )
			headers['Authorization'] = 'Bearer ${token}';
			
		return headers;
	}
	static Future<String> get(String _endpoint) async
	{
		//String qs = Uri(queryParameters: data).query;
		String endpoint 	= apiBase + _endpoint;
		print(endpoint);
		var headers 		= await getHeaders();
		http.Response res 	= await http.get(endpoint, headers: headers);
		//var obj = json.decode(res.body);
		print('StatusCode => ${res.statusCode}');
		if( res.statusCode != 200 )
		{
			throw new RequestException('Ocurrio un error al tratar de recuperar los datos (GET)', res);
		}	
		
		return res.body;
	}
	static Future<String> post(String endpoint, Map<dynamic, dynamic> data) async
	{
		String url		= apiBase + endpoint;
		var headers		= await getHeaders();
		print('headers');print(headers);
		var _json 		= json.encode(data);
		var response 	= await http.post(url, headers: headers, body: _json);
		if( response.statusCode != 200 )
			throw new RequestException('Ocurrio un error al enviar la solicitud (POST)', response);
			
		return response.body;
	}
	static Future<String> put(String endpoint, Map<dynamic, dynamic> data) async
	{
		var headers		= await getHeaders();
		print('headers');print(headers);
		String _json	= json.encode(data);
		var response 	= await http.put(endpoint, headers: headers, body: _json);
		if( response.statusCode != 200 )
			throw new RequestException('Error en la solicitud PUT', response);
			
		return response.body;
	}
}