import 'Service.dart';
import '../Classes/WPUser.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import '../Classes/SB_Settings.dart';
import '../Classes/Exceptions/ExceptionLogin.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class ServiceUsers extends Service
{
	static	bool loggedIn = false;
	
	static Future<WPUser> register(user) async
	{
		var endpoint = Service.apiBase + '/frada/v1/register';
		var headers = {
			'Content-type': 'application/json'
		};
		String data = json.encode(user);
		var response = await http.post(endpoint, headers: headers, body: data);
		print('Status code: ${response.statusCode}');
		
		//if( [200, 201].indexOf(response.statusCode) == -1)
		if( response.statusCode != 200 )
			throw RequestException('Error en el proceso de registro', response);
			
		print(response.body);
		var _json = json.decode(response.body);
			
		var newUser = WPUser.fromJson(_json['data']);
		
		return newUser;
	}
	static Future<WPUser> login(String username, String password) async
	{
		String endpoint = Service.apiBase + '/jwt-auth/v1/token';
		var headers = {
			'Content-type': 'application/json'
		};
		String data = json.encode({'username': username, 'password': password});
		var response = await http.post(endpoint, headers: headers, body: data);
		
		var _json = json.decode(response.body);
		if( response.statusCode != 200 )
		{
			throw new ExceptionLogin('Error de atenticación', _json);
		}
		var user = new WPUser.fromJson(_json);
		print("USER TOKEN: ${_json['token']}");
		startSession(user, _json['token']);
		return user;
	}
	static Future<WPUser> fbLogin(Map<String, dynamic> fb_data) async
	{
		String endpoint = Service.apiBase + '/socialogin/fblogin';
		String data = json.encode(fb_data);
		var res = await http.post(endpoint, body: data, headers: {'Content-type': 'application/json'});
		var _json = json.decode(res.body);
		if( res.statusCode != 200 )
		{
			throw new ExceptionLogin('Error de atenticación', _json);
		}
		var user = new WPUser.fromJson(_json['user']);
		startSession(user, _json['token']);
		SB_Settings.saveString('fb_token', fb_data['token']);
		return user;
	}
	static Future<WPUser> googleLogin(Map<String, dynamic> gdata) async
	{
		String endpoint = Service.apiBase + '/socialogin/googlelogin';
		String data = json.encode(gdata);
		print(endpoint);
		var res = await http.post(endpoint, body: data, headers: {'Content-type': 'application/json'});
		print(res.body);
		var _json = json.decode(res.body);
		if( res.statusCode != 200 )
		{
			throw new ExceptionLogin('Error de atenticación', (_json is Map) ? _json : res.body);
		}
		var user = new WPUser.fromJson(_json['user']);
		startSession(user, _json['token']);
		SB_Settings.saveString('google_token', gdata['token']);
		return user;
	}
	static Future<WPUser> appleLogin(Map<String, dynamic> adata) async
	{
		String endpoint = Service.apiBase + '/socialogin/applelogin';
		String data		= json.encode(adata);
		var res			= await http.post(endpoint, body: data, headers: {'Content-type': 'application/json'});
		var obj			= json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception('Error de autenticación, ${obj['error']}');
		var user = new WPUser.fromJson(obj['user']);
		startSession(user, obj['token']);
		SB_Settings.saveString('apple_token', adata['token']);
		return user;	
	}
	static Future<bool> isLoggedIn() async
	{
		bool authenticated = await SB_Settings.getBool('authenticated');
		if( authenticated == null )
			authenticated = false;
		return authenticated;
	}
	static void startSession(WPUser user, String token)
	{
		//##save token
		SB_Settings.saveString('token', token);
		SB_Settings.saveObject('user', user.toMap());
		SB_Settings.setBool('authenticated', true);
	}
	static void closeSession() async
	{
		await SB_Settings.saveString('token', '');
		await SB_Settings.saveString('user', '');
		await SB_Settings.setBool('authenticated', false);
		String gt = await SB_Settings.getString('google_token');
		String fbt = await SB_Settings.getString('fb_token');
		if( gt != null && !gt.isEmpty )
		{
			print('CLOSING GOOGLE SESSION');
			GoogleSignIn gsi = GoogleSignIn(scopes:['email', 'https://www.googleapis.com/auth/contacts.readonly']);
			//await gsi.disconnect();
			await gsi.signOut();
			await SB_Settings.saveString('google_token', null);
		}
		if( fbt != null && !fbt.isEmpty )
		{
			await SB_Settings.saveString('fb_token', null);
			final facebookLogin = FacebookLogin();
			await facebookLogin.logOut();
		}
	}
	static Future<WPUser> getCurrentUser() async
	{
		var data = await SB_Settings.getObject('user');
		if( data == null )
			return null;
		//print(data);
		var user = WPUser.fromJson(data);
		
		return user;
	}
	static Future<WPUser> me() async
	{
		var data = await SB_Settings.getObject('user');
		if( data == null )
			return null;
		String res = await Service.get('/wp/v2/users/me');
		var obj = json.decode(res);
		
		var user = new WPUser.fromJson(obj);
		return user;
	}
	static Future<WPUser> update(Map<dynamic, dynamic> user) async
	{
		String endpoint = Service.apiBase + '/wp/v2/users/me';
		var headers = await Service.getHeaders();
		String data = json.encode(user);
		var res = await http.post(endpoint, headers: headers, body: data);
		var obj = json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception('Ocurrion un error al tratar de actualizar los datos de usuario');
		var _user = new WPUser.fromJson(obj);
		return _user;
	}
	static Future<Map<String, dynamic>> uploadAvatar(File avatar) async
	{
		String endpoint = Service.apiBase + '/frada/v1/users/avatar';
		var headers = await Service.getHeaders();
		var request = http.MultipartRequest('POST', Uri.parse(endpoint));
		var pic 	= await http.MultipartFile.fromPath('simple-local-avatar', avatar.path);
		request.files.add(pic);
		headers.forEach( (key, value) 
		{
			if( key.toLowerCase() != 'content-type' )
				request.headers[key] = value;
		});
		//request.headers['Content-Type'] = 'multipart/form-data';
		print(endpoint);
		print(request.headers);
		var response 	= await request.send();
		var data 		= await response.stream.toBytes();
		var _json 		= String.fromCharCodes(data);
		print(_json);
		
		return json.decode(_json);
	}
	static Future<WPUser> profile() async
	{
		String endpoint = Service.apiBase + '/wp/v2/users/me';
		var headers 	= await Service.getHeaders();
		var res 		= await http.get(endpoint, headers: headers);
		var _json 		= json.decode(res.body);
		
		if( res.statusCode != 200 )
			throw new Exception('Ocurrio un error al tratar de obtener la informacion del usuario');
		var user = new WPUser.fromJson(_json);
		//print('USER PROFILE');
		//print(user.toMap());
		return user;	
	}
	static Future<void> reload() async
	{
		String token = await SB_Settings.getString('token');
		var user = await profile();
		startSession(user, token);
	}
	static Future<Map> recoverPass(String username) async
	{
		Map<String, String> data = {'username': username};
		String endpoint = Service.apiBase + '/users/forgot';
		var headers 	= await Service.getHeaders();
		var res = await http.post(endpoint, body: json.encode(data), headers: headers);
		print(res.body);
		return json.decode(res.body);
	}
	static Future<Map> changePass(Map data) async
	{
		String endpoint = Service.apiBase + '/wp/v2/users/me';
		print(endpoint);
		var headers 	= await Service.getHeaders();
		var res 		= await http.post(endpoint, body: json.encode(data), headers: headers);
		print(res.body);
		return json.decode(res.body);
	}
	static Future<Map> updateLocation(Map location, [int last = 0]) async
	{
		String endpoint 	= Service.apiBase + '/wp/v2/users/location';
		var headers 		= await Service.getHeaders();
		location['last'] 	= last.toString();
		var res 			= await http.put(endpoint, body: json.encode(location), headers: headers);
		print(res.body);
	}
}
