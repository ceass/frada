import 'Service.dart';
import 'dart:async';
import 'dart:convert';
import '../Classes/WpPost.dart';
import '../Classes/WPUser.dart';
import '../Classes/WPAppointment.dart';

class WpService extends Service
{
	static List<WpPost> _parsePosts(items)
	{
		var posts = <WpPost>[];
		items.forEach((item) => posts.add(WpPost.fromMap(item)));
		
		return posts;
	}
	static Future<List<WpPost>> getServices([int limit = 10]) async
	{
		String endpoint = '/wp/v2/service?per_page=${limit}';
		try
		{
			String response = await Service.get(endpoint);
			var obj = json.decode(response);
			var services = _parsePosts(obj);
			
			return services;
		}
		on RequestException catch(e)
		{
			print(e.message);
			var error = json.decode(e.response.body);
			throw new Exception(error['error']);
		}
		catch(e)
		{
			print(e);
		}
		
	}
	static Future<List<WpPost>> getTopServices([int limit = 10]) async
	{
		String endpoint = '/frada/v1/services/top?per_page=${limit}';
		try
		{
			String response = await Service.get(endpoint);
			var obj = json.decode(response);
			var services = _parsePosts(obj);
			
			return services;
		}
		on RequestException catch(e)
		{
			print('Error recuperando top services');
			print(e.response.body);
			print(e.message);
			var error = json.decode(e.response.body);
			throw new Exception(error['error']);
		}
		catch(e)
		{
			print(e);
		}
	}
	static Future<List> getConsultants([String keyword = '']) async
	{
		String endpoint = '/frada/v1/consultoras';
		if( keyword.isNotEmpty )
			endpoint += '?keyword=${keyword}';
		try
		{
			String response = await Service.get(endpoint);
			var list = json.decode(response);
			var consultants = <WPUser>[];
			list.forEach((item)
			{
				consultants.add( new WPUser.fromJson(item) );
			});
			
			return consultants;
		}
		on RequestException catch(e)
		{
			var error = json.decode(e.response.body);
			throw new Exception(error['error']);
		}
		catch(e)
		{
			print(e);
		}
	}
	static Future<WPUser> getConsultant(int id) async
	{
		String endpoint = '/frada/v1/consultoras/${id}';
		try
		{
			String response = await Service.get(endpoint);
			var obj = json.decode(response);
			print(obj);
			var user = new WPUser.fromJson(obj);
			
			return user;
		}
		on RequestException catch(e)
		{
			
		}
		catch(e)
		{
			
		}
	}
	static Future<WPAppointment> sendAppointment(WPAppointment ap) async
	{
		String endpoint = '/appointments/citas';
		try
		{
			var data		= ap.toMap();
			print(data);
			String response = await Service.post(endpoint, data);
			print(response);
			var _json 		= json.decode(response);
			var newAp 		= WPAppointment.fromMap(_json);
			return newAp;
		}
		on RequestException catch(e)
		{
			var error = json.decode(e.response.body);
			throw new Exception(error['error']);
		}
		catch(e)
		{
			print(e);
			throw e;
		}
	}
	static Future<List> getUserAppointments(WPUser user, [int page = 1]) async
	{
		String endpoint = '/appointments/citas?page=${page}';
		try
		{
			String response = await Service.get(endpoint);
			var data 		= json.decode(response);
			var appointments = <WPAppointment>[];
			data.forEach((apdata)
			{
				appointments.add( new WPAppointment.fromMap(apdata));
			});
			return appointments;
		}
		catch(e)
		{
			print(e);
			throw e;
		}
	}
	static Future<Map> getSettings() async
	{
		String endpoint = '/frada/v1/settings';
		try
		{
			String response = await Service.get(endpoint);
			var data 		= json.decode(response);
			return data;
		}
		catch(e)
		{
			print(e);
			throw e;
		}
	}
}