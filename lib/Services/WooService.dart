import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import '../Classes/WCProduct.dart';
import '../Classes/WCOrder.dart';
import '../Classes/WCCategory.dart';
import '../Classes/WCShippingZone.dart';
import '../Classes/WCShippingMethod.dart';
import 'Service.dart';
import 'ServiceUsers.dart';

class WooService
{
	static String _clientKey 	= 'ck_89bf9d0ca59c23b9c4780f011abe67c854b7510e';
	static String _secretKey	= 'cs_1d17864ee0e8b96759bc5384f3cdc1d51571d875';
	static String baseUrl		= 'https://www.fradaswissco.com';
	static String _apiBase		= 'https://www.fradaswissco.com/wp-json/wc/v3';
	
	static Map<String, String> _getHeaders()
	{
		var headers = {
			'Content-Type': 'application/json',
		};
		if( 1 == 2 )
		{
			headers[HttpHeaders.authorizationHeader] = 'Basic ${_clientKey}:${_secretKey}';
		}
		print(headers);
		return headers;
	}
	static Future<List> _get(String _endpoint) async
	{
		//String qs = Uri(queryParameters: data).query;
		String endpoint = _apiBase + _endpoint;
		if( endpoint.indexOf('?') != -1 )
			endpoint += '&';
		else 
			endpoint += '?';  
		endpoint += 'consumer_key=${_clientKey}&consumer_secret=${_secretKey}';
		var headers = _getHeaders();
		print(endpoint);
		http.Response res = await http.get(endpoint/*, headers: headers*/);
		var obj = json.decode(res.body);
		if( res.statusCode != 200 )
		{
			print(obj['message']);
			throw new Exception('Ocurrio un error al tratar de recuperar los productos');
		}	
		
		return obj;
		
	}
	static Future<String> post(endpoint, Map<dynamic, dynamic> data) async
	{
		if( endpoint.indexOf('?') != -1 )
			endpoint += '&';
		else 
			endpoint += '?';  
		endpoint += 'consumer_key=${_clientKey}&consumer_secret=${_secretKey}';
		String url			= _apiBase + endpoint;
		var headers = _getHeaders();
		String body	= json.encode(data);
		http.Response res = await http.post(url, headers: headers, body: body);
		print('POST RESPONSE CODE: ${res.statusCode}');
		
		if( [200, 201].indexOf(res.statusCode) == -1 )
		{
			throw new RequestException('Ocurrio un error al al enviar la solicitud post', res);
		}
		return res.body;
	}
	static Future<List> getProducts([int page = 1, int limit = 10, int catId = 0]) async
	{
		String endpoint = '/products?page=${page}&per_page=${limit}';
		if( catId > 0 )
		{
			endpoint += '&category=${catId}';
		}
		
		var products = await _get(endpoint);
		
		return _parseProducts(products);
	}
	static Future<List> search(String keyword) async
	{
		String endpoint = '/products?search=${keyword}';
		var products = await _get(endpoint);
		
		return _parseProducts(products);
	}
	static List<WCProduct> _parseProducts(List data)
	{
		var products = new List<WCProduct>();
		data.forEach( (obj) 
		{
			products.add( WCProduct.fromJson(obj) );
		});
		
		return products;
	}
	static Future<List> getFeaturedProducts([int limit = 10]) async
	{
		var products = await _get('/products?featured=true&limit=${limit}');
		
		return _parseProducts(products);
	}
	static Future<WCOrder> sendOrder(WCOrder order) async
	{
		String endpoint = '/orders';
		try
		{
			var order_data = order.toMap();
			print(order_data);
			String _json = await post(endpoint, order_data);
			var obj = json.decode(_json);
			
			return WCOrder.fromMap(obj);
		}
		on RequestException catch(e)
		{
			print('ERROR al procesar el pedido');
			print(e.message);
			print(e.response.body);
			throw new Exception('Ocurrio un error al procesar el pedido, intentelo nuevamente');
		}
		catch(e)
		{
			print('Error desconocido el enviar el pedido');
			print(e);
		}
		
	}
	static Future<List> getOrders([int page = 1, int limit = 10]) async
	{
		var user = await ServiceUsers.getCurrentUser();
		String endpoint = '/orders?customer=${user.ID}&page=${page}&per_page=${limit}';
		try
		{
			var _list = await _get(endpoint);
			var orders = <WCOrder>[];
			_list.forEach((od)
			{
				orders.add(new WCOrder.fromMap(od));
			});
			
			return orders;
		}
		on RequestException catch(e)
		{
			throw new Exception(e.message);	
		}
		catch(e)
		{
			print('Error desconocido el obtener los pedidos');
			print(e);
			throw e;
		}
	}
	static Future<List> getCategories([int parent = 0, int limit = 20, int page = 1, bool hide_empty = true]) async
	{
		String endpoint = '/products/categories?page=${page}&per_page=${limit}&parent=$parent&hide_empty=${hide_empty}';
		try
		{
			var _list = await _get(endpoint);
			var categories = <WCCategory>[];
			_list.forEach((cd)
			{
				categories.add(new WCCategory.fromMap(cd));
			});
			
			return categories;
		}
		catch(e)
		{
			throw e;
		}
	}
	static Future<List> getShippingZones() async
	{
		String endpoint = '/shipping/zones';
		try
		{
			var list = await _get(endpoint);
			var zones = <WCShippingZone>[];
			list.forEach((z) => zones.add(new WCShippingZone.fromMap(z)));
			
			return zones;
		}
		catch(e)
		{
			throw e;
		}
	}
	static Future<List> getShippingMethods(int zoneId) async
	{
		String endpoint = '/shipping/zones/${zoneId}/methods';
		try
		{
			var list = await _get(endpoint);
			var methods = <WCShippingMethod>[];
			list.forEach((m) => methods.add(new WCShippingMethod.fromMap(m)));
			
			return methods;
		}
		catch(e)
		{
			throw e;
		}
	}
}