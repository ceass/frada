import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:provider/provider.dart';
//import 'package:transparent_image/transparent_image.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'dart:convert';

import 'Widgets/Users/WidgetLogin.dart';
import 'Services/WooService.dart';
import 'Widgets/WidgetSideDrawer.dart';
import 'Classes/WCProduct.dart';
import 'Classes/WpPost.dart';
import 'Widgets/Products/WidgetSingleProduct.dart';
import 'Widgets/Services/WidgetServices.dart';
import 'Services/ServiceUsers.dart';
import 'Providers/CartProvider.dart';
import 'Classes/SB_Globals.dart';
import 'Classes/SB_Settings.dart';
import 'Widgets/WidgetBottomMenu.dart';
import 'Services/WpService.dart';
import 'Widgets/Services/WidgetService.dart';

void main() 
{
	runApp(
		MultiProvider(
			providers: [
				//ChangeNotifierProvider(create: (_) => CommonProvider()),
				ChangeNotifierProvider(create: (_) => CartProvider())
			],
			child: MyApp()
		)
	);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
	@override
	Widget build(BuildContext context) 
	{
		var color = MaterialColor(0xffFF4C58, {
			50: Color(0x00FF4C58),
			100: Color(0x00FF4C58),
			200: Color(0x00FF4C58),
			300: Color(0x00FF4C58),
			400: Color(0x00FF4C58),
			500: Color(0xffFF4C58),
			600: Color(0x00FF4C58),
			700: Color(0x00FF4C58),
			800: Color(0x00FF4C58),
			900: Color(0x00FF4C58),
			
		});
		color = MaterialColor(0xfff2b2b2, {
			50: Color(0xfff2b2b2),
			100: Color(0xfff2b2b2),
			200: Color(0xfff2b2b2),
			300: Color(0xfff2b2b2),
			400: Color(0xfff2b2b2),
			500: Color(0xfff2b2b2),
			600: Color(0xfff2b2b2),
			700: Color(0xfff2b2b2),
			800: Color(0xfff2b2b2),
			900: Color(0xfff2b2b2),
		});
	    return MaterialApp(
			title: 'Frada',
			theme: ThemeData(
		        // This is the theme of your application.
		        //
		        // Try running your application with "flutter run". You'll see the
		        // application has a blue toolbar. Then, without quitting the app, try
		        // changing the primarySwatch below to Colors.green and then invoke
		        // "hot reload" (press "r" in the console where you ran "flutter run",
		        // or simply save your changes to "hot reload" in a Flutter IDE).
		        // Notice that the counter didn't reset back to zero; the application
		        // is not restarted.
		        primarySwatch: color,
		        // This makes the visual density adapt to the platform that you run
		        // the app on. For desktop platforms, the controls will be smaller and
		        // closer together (more dense) than on mobile platforms.
		        visualDensity: VisualDensity.adaptivePlatformDensity,
				fontFamily: 'Biko',
				bottomAppBarTheme: BottomAppBarTheme(
					color: Color(0xffffd6d1),
				)
	      	),
			home: MyHomePage(),
			/*
			home: FutureBuilder(
				future: ServiceUsers.isLoggedIn(),
				builder: (ctx, snapshot)
				{
					if( snapshot.hasData )
					{
						if( snapshot.data )
						{
							return MyHomePage(title: 'Frada');
						}
						return WidgetLogin();
					}
					
					return Center(child: CircularProgressIndicator());
					//
				},
			),
			*/
			debugShowCheckedModeBanner: false,
			localizationsDelegates: [
				GlobalMaterialLocalizations.delegate,
				GlobalWidgetsLocalizations.delegate,
			],
			supportedLocales: [
				const Locale('en', 'US'),
				const Locale('es', 'ES'),
			],
	    );
	}
}

class MyHomePage extends StatefulWidget 
{
	MyHomePage(); //: super(key: key);
	@override
	_MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> 
{
	String			appTitle = 'Frada';
	List			sliders = [];
	Map				settings = {};
	
	void _getLocation() async
	{
		print('Getting location');
		//Geolocator().checkGeolocationPermissionStatus();
		getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
			.then( (position) 
			{
				print(position);
				String _json = json.encode({
					'lat': position.latitude.toString(), 
					'lng': position.longitude.toString()
				});
				SB_Settings.saveString('location', _json);
			})
			.catchError( (e)
			{
				print("Get position error");
				print(e);
			});
		var locationOptions = {};
		var positionStream = getPositionStream().listen((Position position) async
		{
			print('Location/Position changed');
			print(position);
			if( await ServiceUsers.isLoggedIn() )
			{
				print('Updating user location');
				ServiceUsers.updateLocation({'lat': position.latitude.toString(), 'lng': position.longitude.toString()}, 1);
			}
		});
	}
	Widget _getServices(double bannerHeight)
	{
		//var settings = null;
		//SB_Settings.getObject('settings').then((d) => settings = d);
		return FutureBuilder(
			future: WpService.getTopServices(6),
			builder: (ctx, snapshot)
			{
				if( snapshot.hasData )
				{
					return Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						children: [
							if( this.settings != null && this.settings['service_image'] != null )
								Image.network(this.settings['service_image'], height: bannerHeight,  fit: BoxFit.fitWidth,),
							//else
							//	Image.asset('images/services.jpg', height: bannerHeight,  fit: BoxFit.fitWidth,),
							SizedBox(height: 10),
							Container(
								padding: EdgeInsets.all(8),
								child: Text('Servicios Destacados'.toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
							),
							SizedBox(height: 10),
							Container(
								height: 200,
								child: ListView.builder(
									scrollDirection: Axis.horizontal,
									itemCount: snapshot.data.length,
									itemBuilder: (ctx, index)
									{
										var service = (snapshot.data[index] as WpPost);
										return Card(
											child: Container(
												width: 180,
												child: InkWell(
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.stretch,
														children: [
															/*
															Center(
																child: ClipRRect(
																	borderRadius: BorderRadius.circular(80),
																	child: Image.network(service.featuredImage, fit: BoxFit.cover, height: 90,)
																)
															),
															*/
															Expanded(
																child: Image.network(service.featuredImage, fit: BoxFit.cover, height: 90,)
															),
															Container(
																padding: EdgeInsets.all(8),
																child: Text(service.title, textAlign: TextAlign.center, softWrap: true, overflow: TextOverflow.clip,),
															)
														]
													),
													onTap: ()
													{
														Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetService(service: service)));
													},
												)
											)
										);
									}
								)
							),
							Center(
								child: FlatButton(
									color: Color(0xffFF4C58),
									child: Text('Ver nuestros servicios', style: TextStyle(color: Colors.white, fontSize: 22)),
									onPressed: ()
									{
										Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetServices()));
									},
								)
							)
						]
					);
				}
				return Center(
					child: Column(
						children: [
							Text('Obteniendo servicios..'),
							SizedBox(height: 8),
							CircularProgressIndicator()
						]
					)
				);
			}
		);
	}
	Widget _getHomeWidgets(double bannerHeight)
	{
		//var settings = SB_Settings.getObject('settings');
		//print(settings);
		return Container(
			color: Colors.white,
			child: ListView(
				children: [
					this._getSlider(bannerHeight),
					SizedBox(height: 10),
					this._getServices(bannerHeight),
					SizedBox(height: 10),
					this._getBanners(bannerHeight),
					SizedBox(height: 20),
					this._featuredProducts(),
					SizedBox(height: 10),
				]
			)
		);
	}
	@override
	void initState()
	{
		this._getLocation();
		super.initState();
		//WooService.getProducts();
	}
  	@override
  	Widget build(BuildContext context) 
	{
		var size = MediaQuery.of(context).size;
		print('Device: ${size.width}x${size.height}');
		double bannerHeight = size.width > 800 ? 300 : 220;
		print('Banners height: ${bannerHeight}');
		
		return FutureBuilder(
			future: WpService.getSettings(),
			builder: (ctx, snapshot)
			{
				if( snapshot.hasData )
				{
					SB_Settings.saveObject('settings', snapshot.data);
					this.settings = snapshot.data;
					this.sliders = snapshot.data['sliders'] ?? [];
					this.appTitle = snapshot.data['app_title'];
					return Scaffold(
						appBar: AppBar(
				        	title: Text(this.appTitle),
						),
						drawer: WidgetSideDrawer(),
						bottomNavigationBar: WidgetBottomMenu(currentIndex: 0),
						body: this._getHomeWidgets(bannerHeight),
				    );
				}
				return Scaffold(
					body: Center(child: Column(
						mainAxisAlignment: MainAxisAlignment.center,
						children: [
							Text('Cargando datos...'),
							SizedBox(height: 8),
							CircularProgressIndicator()
						]
					))
				);
			},
		);
	}
	Widget _getBanners([double height = null])
	{
		height = height * 1.40;
		if( this.settings != null && this.settings['home_banner'] != null )
		{
			return Container(
				height: height,
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Expanded(
							child: Container(
								child: Image.network(this.settings['home_banner'], fit: BoxFit.cover)
							),
						),
						Container(
							padding: EdgeInsets.all(8),
							child: Text(settings['home_banner_text'] ?? '', style: TextStyle(fontSize: 18))	
						),
					]
				),
			);
		}
		return Stack(
			children: [
				Container(
					child: Column(
						children: [
							Container(
								color: Colors.black,
								child: Row(
									children:[
										RotatedBox(
											quarterTurns: 3,
											child: Container(
												
												padding: EdgeInsets.only(top: 10, right: 5, bottom: 5, left: 5),
												child: Text('Promoción', style: TextStyle(color: Colors.white, fontSize: 30))
											)
										),
										Expanded(
											child: Image.asset('images/promo.jpg', height: height, fit: BoxFit.fitWidth),
										)
									]
								), 
							),
							Container(
								padding: EdgeInsets.all(10),
								child: Text('Durante esta cuarentena puedes obtener un descuento del 10% en tu proxima compra',
									textAlign: TextAlign.right,
									style: TextStyle(fontSize: 15)
								),
							),
						]
					)
				),
				Positioned.fill(
					/*top: 20,
					right: 0,
					bottom: 0,*/
					left: 65,//45,
					child: Container(
						//color: Colors.red,
						width: 80,
						child: Column(
							crossAxisAlignment: CrossAxisAlignment.stretch,
							mainAxisAlignment: MainAxisAlignment.center,
							children: [Text('Descuento del 10% \nen nuestros cursos', style: TextStyle(fontSize: 22))]
						)
					)
				)
			]
		);
	}
	Widget _featuredProducts()
	{
		var size = MediaQuery.of(context).size;
		
		double _height = size.width > 800 ? 270 : 230;
		double _width = size.width > 800 ? 270 : 230;
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			children: [
				Container(
					padding: EdgeInsets.only(right: 10, left: 10),
					child: Text('Productos Destacados'.toUpperCase(), style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
				),
				SizedBox(height: 10),
				FutureBuilder(
					future: WooService.getFeaturedProducts(),
					builder: (ctx, snapshot)
					{
						if( snapshot.hasData )
						{
							return Container(
								height: _height,
								child: ListView.builder(
									scrollDirection: Axis.horizontal,
									itemCount: snapshot.data.length,
									itemBuilder: (_, index)
									{
										var product = (snapshot.data[index] as WCProduct);
										var iw = InkWell(
											child: Container(
												margin: EdgeInsets.all(8),
												//padding: EdgeInsets.all(8),
												width: _width,
												height: _height,
												child: Column(
													crossAxisAlignment: CrossAxisAlignment.stretch,
													children: [
														Container(
															height: _height * 0.6,
															width: _width,
															child: FadeInImage.assetNetwork(
																placeholder: 'images/spin.gif',
																image: product.getThumbnailUrl(),
																fit: BoxFit.contain,
															)
														),
														SizedBox(height: 8),
														Expanded(
															child: Container(
																padding: EdgeInsets.all(8),
																child: Text(product.name, style: TextStyle(fontSize: 15))
															),
														),
														Text(product.getPrice(), textAlign: TextAlign.center, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
														SizedBox(height: 8),
													]
												)
											),
											onTap: ()
											{
												Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetSingleProduct(product: product)));
											}
										);
										
										return Card(child: iw);
									},
								)
							);
						}
						else if( snapshot.hasError )
						{
							return Center(child: Text('Ocurrion un problema al recuperar los productos destacados'));
						}
						return Center(child: CircularProgressIndicator());
					}
				)
			]
		);
	}
	Widget _getSlider([double _height = 190])
	{
		if( _height == null )
			_height = 190;
		return Container(
			//height: 250,
			color: Colors.grey,
			child: CarouselSlider(
				options: CarouselOptions(
					height: _height,
					viewportFraction: 1.0,
					enlargeCenterPage: false,
					autoPlay: true,
					autoPlayInterval: Duration(seconds: 5)
				),
				items: this.sliders.map((slide)
				{
					return Container(
						width: MediaQuery.of(context).size.width,
						child: Stack(
							children: [
								Image.network(slide['src'], fit: BoxFit.fill, height: _height,),
								/*
								Positioned.fill(
									child: Image.network(slide['image'], fit: BoxFit.fill, height: _height,),
								)
								*/
								Positioned(
									top: 50,
									left: 10,
									child: Container(
										width: MediaQuery.of(context).size.width / 2,
										child: Text(slide['text'], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
									)
								)
							]
						)
					);
					/*
					return Builder(
						builder: (ctx)
						{
							return Container(
								width: MediaQuery.of(context).size.width,
								child: Stack(
									children: [
										Positioned.fill(
											child: Image.network(slide['image'], fit: BoxFit.cover,),
										)
									]
								)
							);
						}
					);
					*/
				}).toList()
			)
		);
	}
}